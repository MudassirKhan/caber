package com.muverity.caber.order;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.model.Order_Model;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Current_Adapter extends RecyclerView.Adapter<Current_Adapter.ViewHolder>{

    private static int firstVisible = 0;
    ArrayList<Order_Model.current_ride_info> list = new ArrayList<>();
    Context context;
    String monthName;
    PreferenceUtils pref;

    public Current_Adapter(Context appContext, ArrayList<Order_Model.current_ride_info>  currentlist) {
        this.list = currentlist;
        this.context = appContext;
        pref = new PreferenceUtils(context);
    }

    @NonNull
    @Override
    public Current_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.current_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull Current_Adapter.ViewHolder holder, final int position) {


        if(list.get(position).getStatus().equals("Pending")){
            holder.btn_join.setText("Joining today");
        }else if(list.get(position).getStatus().equals("Accepted")){
            holder.btn_join.setText("Joining today");
        }else if(list.get(position).getStatus().equals("Confirmed")){
            holder.btn_join.setText("Complete");
        }else if(list.get(position).getStatus().equals("Completed")){
            holder.btn_join.setText("Completed");
        }else {
            holder.btn_join.setText("");
        }


        holder.tv_direction.setText("Ride with Alex newton");
        holder.tv_carname.setText(list.get(position).getCar_type());

        holder.btn_join.setOnClickListener(new View.OnClickListener() {
        @Override
             public void onClick(View v) {

            if(pref.getrole().equals("1")){

                if(list.get(position).getStatus().equals("Pending") || list.get(position).getStatus().equals("Accepted")){

                    final NavController navController = Navigation.findNavController((Activity) context, R.id.my_nav_host_fragment);

                    Bundle args = new Bundle();
                    args.putInt("ride_id", list.get(position).getId());
                    args.putString("budget", list.get(position).getBudget());
                    navController.navigate(R.id.order_fragment_to_result_fragment , args);

                }else {

                  /*  final NavController navController = Navigation.findNavController((Activity) context, R.id.my_nav_host_fragment);
                    navController.navigate(R.id.order_fragment_to_accepted_fragment);*/

                }

            }
        }
        });



                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date parsedDate = null;
                try {
                    parsedDate = dateFormat.parse(list.get(position).getCreated_at());

                    String dayOfTheWeek = (String) DateFormat.format("EEEE",parsedDate); // Thursday
                    String day          = (String) DateFormat.format("dd", parsedDate); // 20
                    String monthString  = (String) DateFormat.format("MMM", parsedDate); // Jun
                    String monthNumber  = (String) DateFormat.format("MM",parsedDate); // 06
                    String year         = (String) DateFormat.format("yyyy",parsedDate);
                    String time         = (String) DateFormat.format("hh:mm a",parsedDate);

                    Log.e("TAG" , "TIME_STAMP ="+dayOfTheWeek );
                    Log.e("TAG" , "TIME_STAMP ="+day );
                    Log.e("TAG" , "TIME_STAMP ="+monthString );
                    Log.e("TAG" , "TIME_STAMP ="+monthNumber );
                    Log.e("TAG" , "TIME_STAMP ="+year );
                    Log.e("TAG" , "TIME_STAMP ="+time );

                    holder.tv_date.setText(monthString + " "+ day);
                    holder.tv_time.setText(time);

                } catch (ParseException e) {
                    e.printStackTrace();
                }



     //   String myTimestamp= str_date;



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView tv_date , tv_direction ,tv_carname ,tv_time;
        private View view;
        Button btn_join;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_time = itemView.findViewById(R.id.tv_time);
            tv_carname = itemView.findViewById(R.id.tv_carname);
            tv_direction = itemView.findViewById(R.id.tv_direction);
            tv_date = itemView.findViewById(R.id.tv_date);
            btn_join = itemView.findViewById(R.id.btn_join);
            this.view=itemView;
        }
        public View getView() {
            return view;
        }
    }

}
