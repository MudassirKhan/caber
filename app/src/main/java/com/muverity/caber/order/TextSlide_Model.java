package com.muverity.caber.order;

public class TextSlide_Model {
    private String names;
    private boolean selected;

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public TextSlide_Model(String names, boolean selected) {
        this.names = names;
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
