package com.muverity.caber.order;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.muverity.caber.R;
import com.muverity.caber.api.PreferenceUtils;

public class TextSlide_Adapter extends RecyclerView.Adapter<TextSlide_Adapter.ViewHolder>{

    private static int firstVisible = 0;
    private TextSlide_Model[] listdata;
    private Context context;
    PreferenceUtils pref;

    public TextSlide_Adapter(Context context, TextSlide_Model[] listdata) {
        this.listdata = listdata;
        this.context = context;
        pref = new PreferenceUtils(context);
    }

    @NonNull
    @Override
    public TextSlide_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.textslide_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull TextSlide_Adapter.ViewHolder holder, final int position) {

        holder.tv_username.setText(listdata[position].getNames());
        if(listdata[position].isSelected()) {
            holder.tv_username.setTextColor(context.getResources().getColor(R.color.gray));
            holder.getView().setAnimation(AnimationUtils.loadAnimation(context,R.anim.zoomin));
            holder.tv_username.setTypeface(null, Typeface.BOLD);

            if(pref.getrole().equals("1")){
                OrderMain_Fragment.previous();
            }else {
                DriverOrderMain_Fragment.previous();
            }

        } else {
            holder.tv_username.setTextColor(context.getResources().getColor(R.color.gray));
            holder.tv_username.setTypeface(null, Typeface.NORMAL);
            holder.getView().setAnimation(AnimationUtils.loadAnimation(context,R.anim.zoomout));

            if(pref.getrole().equals("1")){
                OrderMain_Fragment.current();
            }else {
                DriverOrderMain_Fragment.current();
            }

        }

        holder.tv_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i=0;i<listdata.length;i++) {
                    listdata[i].setSelected(i==position);
                }
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView tv_username;
        private View view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_username = itemView.findViewById(R.id.tv_username);
            this.view=itemView;
        }
        public View getView() {
            return view;
        }
    }

}
