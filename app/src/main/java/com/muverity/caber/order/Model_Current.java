package com.muverity.caber.order;

public class Model_Current {

    private String date;
    private String direction;
    private String carname;
    private String time;

    public Model_Current(String date, String direction, String carname, String time) {
        this.date = date;
        this.direction = direction;
        this.carname = carname;
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCarname() {
        return carname;
    }

    public void setCarname(String carname) {
        this.carname = carname;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
