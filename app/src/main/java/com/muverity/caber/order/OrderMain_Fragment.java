package com.muverity.caber.order;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.MyApplication;
import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.Order_Model;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class OrderMain_Fragment extends Fragment {


    public static ArrayList<Order_Model.current_ride_info> current_list = new ArrayList<>();
    public static ArrayList<Order_Model.previous_ride_info> previous_list = new ArrayList<>();

    public static   RecyclerView recyclerview_textslide ;
    public static   RecyclerView recyclerview_rvorder ;
    public static   TextSlide_Adapter textSlide_adapter_adapter;
    public static   Current_Adapter current_adapter;
    public static   Previous_Adapter previous_adapter;

    private static Context context = null;
    LinearLayout ll_chat , ll_profile;
    ImageView iv_back;
    public static PreferenceUtils pref;

    TextSlide_Model[] textslide_items = new TextSlide_Model[] {

            new TextSlide_Model("Current Order", true),
            new TextSlide_Model("Previous Order", false),
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ordermain_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        iv_back = view.findViewById(R.id.iv_back);
        pref = new PreferenceUtils(getActivity());
        recyclerview_textslide = (RecyclerView) view.findViewById(R.id.recyclerview_textslide);
        recyclerview_rvorder = (RecyclerView) view.findViewById(R.id.recyclerview_rvorder);
        context=getActivity();

        ll_chat = view.findViewById(R.id.ll_chat);
        ll_profile = view.findViewById(R.id.ll_profile);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        textSlide_adapter_adapter = new TextSlide_Adapter(getContext(), textslide_items);
        recyclerview_textslide.setHasFixedSize(true);
        recyclerview_textslide.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL, false));
        recyclerview_textslide.setAdapter(textSlide_adapter_adapter);

        recyclerview_rvorder.setHasFixedSize(true);
        recyclerview_rvorder.setLayoutManager(new LinearLayoutManager(MyApplication.getAppContext() , LinearLayoutManager.VERTICAL, false));

        recyclerview_rvorder.setHasFixedSize(true);
        recyclerview_rvorder.setLayoutManager(new LinearLayoutManager(MyApplication.getAppContext() , LinearLayoutManager.VERTICAL, false));

        current_adapter = new Current_Adapter(MyApplication.getAppContext() , current_list);
        recyclerview_rvorder.setHasFixedSize(true);
        recyclerview_rvorder.setLayoutManager(new LinearLayoutManager(MyApplication.getAppContext() , LinearLayoutManager.VERTICAL, false));
        recyclerview_rvorder.setAdapter(current_adapter);

        recyclerview_textslide.post(new Runnable()
        {
            @Override
            public void run() {
                textSlide_adapter_adapter.notifyDataSetChanged();
            }
        });

        ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.order_fragment_to_chat_fragment);
            }
        });

        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(pref.getrole().equals("1")){
                    navController.navigate(R.id.order_fragment_to_pasenger_profile);
                }
            }
        });

        current();


    }

    public static void current(){

        try {
            if (Utils.isNetworkAvailable(context)) {
                Utils.showProgress1("Please wait",context);
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.current_previous_list(
                        "Token "+ pref.getauthtoke(),
                        new Callback<Order_Model>() {
                    @Override
                    public void success(Order_Model modelLeaves, Response response) {
                        Utils.hideProgress1();
                        previous_list.clear();
                        current_list.clear();
                        current_list.addAll(modelLeaves.getCurrent_ride());
                        current_adapter = new Current_Adapter(context , current_list);
                        recyclerview_rvorder.setAdapter(current_adapter);
                        current_adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("TAG" , "TEST_ERROR" + error);
                        Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
                        Utils.hideProgress1();
                    }
                });

            } else {
                Toast.makeText(context, "Please connect to an internet", Toast.LENGTH_SHORT).show();
            }
            Utils.hideProgress1();
        } catch (Exception e) {
            Utils.hideProgress1();
            e.printStackTrace();
        }


    }

    public static void previous(){
        try {
            if (Utils.isNetworkAvailable(context)) {
                Utils.showProgress1("Please wait",context);
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.current_previous_list(
                        "Token "+ pref.getauthtoke(),
                        new Callback<Order_Model>() {
                            @Override
                            public void success(Order_Model modelLeaves, Response response) {

                                Utils.hideProgress1();
                                current_list.clear();
                                previous_list.clear();
                                previous_list.addAll(modelLeaves.getPrevious_ride());
                                previous_adapter = new Previous_Adapter(context , previous_list);
                                recyclerview_rvorder.setAdapter(previous_adapter);
                                previous_adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Log.e("TAG" , "TEST_ERROR" + error);
                                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }

                        });

            } else {
                Toast.makeText(context, "Please connect to an internet", Toast.LENGTH_SHORT).show();
            }
            Utils.hideProgress1();
        } catch (Exception e) {
            Utils.hideProgress1();
            e.printStackTrace();
        }


       /* previous_adapter = new Previous_Adapter(MyApplication.getAppContext(), previouslist);
        recyclerview_rvorder.setHasFixedSize(true);
        recyclerview_rvorder.setLayoutManager(new LinearLayoutManager(MyApplication.getAppContext() , LinearLayoutManager.VERTICAL, false));
        recyclerview_rvorder.setAdapter(previous_adapter);
*/
    }

    @Override
    public void onResume() {
        super.onResume();
        current();
    }

}
