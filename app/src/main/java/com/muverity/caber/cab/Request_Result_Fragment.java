package com.muverity.caber.cab;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.adapter.ReqResult_Adapter;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.Accetpt_Ride_Model;
import com.muverity.caber.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Request_Result_Fragment extends Fragment {

    LinearLayout ll_chat , ll_profile ;
    ImageView img_order , iv_back , img_bell;
    RecyclerView rv_acceptedList;
    ReqResult_Adapter acceptrideadapter ;
    ArrayList<Accetpt_Ride_Model> list = new ArrayList<Accetpt_Ride_Model>();
    PreferenceUtils pref;
    int ride_id ;
    String budget;
    TextView tv_name;
    ImageView img_main;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.request_result_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        rv_acceptedList = (RecyclerView)view. findViewById(R.id.rv_acceptedList);
        rv_acceptedList.setHasFixedSize(true);
        rv_acceptedList.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.VERTICAL, false));

        ll_chat = view.findViewById(R.id.ll_chat);
        ll_profile = view.findViewById(R.id.ll_profile);
        img_order = view.findViewById(R.id.img_order);
        tv_name = view.findViewById(R.id.tv_name);
        iv_back = view.findViewById(R.id.iv_back);
        img_main = view.findViewById(R.id.img_main);
        img_bell = view.findViewById(R.id.img_bell);
        pref = new PreferenceUtils(getActivity());

        ride_id = getArguments().getInt("ride_id");
        budget = getArguments().getString("budget");

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        if (pref.getprofile_pic() != null) {
            if (!pref.getprofile_pic().equals("")) {
                Picasso.with(getActivity()).load(pref.getprofile_pic()).placeholder(R.drawable.default_profile).into(img_main);
            }
        }

        if (pref.getfirstname() != null) {
            if (!pref.getfirstname().equals("")) {
                tv_name.setText("Hey"+" "+pref.getfirstname());
            }
        }

        ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.result_fragment_to_chat_fragment);
            }
        });

        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.result_fragment_to_pasenger_profile);
            }
        });

        img_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.result_fragment_to_order_fragment);
            }
        });

        img_bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.result_fragment_to_notification_fragment);
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        accept_driver_list();
    }

    private void acceptride() {

        try {
            if (Utils.isNetworkAvailable(getActivity())) {
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.accept_ride(
                        "Token "+ pref.getauthtoke(),
                        ride_id,
                        budget,
                        new Callback<Accetpt_Ride_Model>() {
                            @Override
                            public void success(Accetpt_Ride_Model modelLeaves, Response response) {
                                Utils.hideProgress1();

                                list.clear();
                               // list.addAll(modelLeaves.getLeave());
                                acceptrideadapter.notifyDataSetChanged();

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Utils.hideProgress1();
                            }
                        });
            } else {
                Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void accept_driver_list() {
        try {

            if (Utils.isNetworkAvailable(getActivity())) {

                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.list_of_accepted_driver(
                        "Token "+ pref.getauthtoke(),
                         ride_id,
                        new Callback< ArrayList<Accetpt_Ride_Model>>()  {
                            @Override
                            public void success(ArrayList<Accetpt_Ride_Model> modelLeaves, Response response) {

                                if(response.getStatus() == 200){

                                    Utils.hideProgress1();
                                    list.clear();
                                    list = modelLeaves ;
                                    acceptrideadapter = new ReqResult_Adapter(getActivity(), list);
                                    rv_acceptedList.setAdapter(acceptrideadapter);
                                    acceptrideadapter.notifyDataSetChanged();

                                }else {
                                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Log.e("TAG" , "ERROR_TEST" + error);
                                Toast.makeText(getActivity(), "falure", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }

                        });
            } else {
                Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}


