package com.muverity.caber.cab;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.MyApplication;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.Cab_Model;
import com.muverity.caber.R;
import com.muverity.caber.adapter.Caber_Adapter;
import com.muverity.caber.model.Create_Ride_Model;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class Cab_Fragment extends Fragment {

    LinearLayout btn_place;
    ImageView img_bell , img_order ;
    LinearLayout ll_chat , ll_profile;
    String pickup_name = "";
    String pickup_address = "";
    String pickup_latitude = "";
    String pickup_longitude = "";
    String drop_name = "";
    String drop_address = "";
    String drop_latitude = "";
    String drop_longitude = "";
    String cab = "";
    PreferenceUtils pref;
    EditText et_ride_amt;
    NavController navController ;

    Cab_Model[] myListData = new Cab_Model[]{

            new Cab_Model("Economy", R.drawable.ic_car1),
            new Cab_Model("Prime", R.drawable.ic_car2),
            new Cab_Model("Xl", R.drawable.ic_car1),
    };

    ImageView iv_back;


    ImageView img_main ;
    TextView tv_name;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cab_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
        pref = new PreferenceUtils(getActivity());

        img_main = view.findViewById(R.id.img_main);
        tv_name = view.findViewById(R.id.tv_name);

        if (pref.getprofile_pic() != null) {
            if (!pref.getprofile_pic().equals("")) {
                Picasso.with(getActivity()).load(pref.getprofile_pic()).placeholder(R.drawable.default_profile).into(img_main);
            }
        }

        if (pref.getfirstname() != null) {
            if (!pref.getfirstname().equals("")) {
                tv_name.setText("Hey"+" "+pref.getfirstname());
                //  et_name.setText(pref.getfirstname() + " "+ pref.getlastname());
            }
        }


        pickup_name = getArguments().getString("pickup_name");
        pickup_address = getArguments().getString("pickup_address");
        pickup_latitude = getArguments().getString("pickup_latitude");
        pickup_longitude = getArguments().getString("pickup_longitude");
        drop_name = getArguments().getString("drop_name");
        drop_address = getArguments().getString("drop_address");
        drop_latitude = getArguments().getString("drop_latitude");
        drop_longitude = getArguments().getString("drop_longitude");
        cab = getArguments().getString("cab");

        Log.e("TAG","Address_Pname"+pickup_name);
        Log.e("TAG","Address_Paddress"+pickup_address);
        Log.e("TAG","Address_Paddress"+drop_name);
        Log.e("TAG","Address_Paddress"+drop_address);

        btn_place = view.findViewById(R.id.btn_place);
        img_order = view.findViewById(R.id.img_order);
        img_bell = view.findViewById(R.id.img_bell);
        ll_chat = view.findViewById(R.id.ll_chat);
        ll_profile = view.findViewById(R.id.ll_profile);
        et_ride_amt = view.findViewById(R.id.et_ride_amt);
        iv_back = view.findViewById(R.id.iv_back);

        RecyclerView rv_profile_cars = (RecyclerView)view. findViewById(R.id.rv_profile_cars);
        Caber_Adapter language_adapter = new Caber_Adapter(getActivity() , myListData);
        rv_profile_cars.setHasFixedSize(true);
        rv_profile_cars.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.VERTICAL, false));
        rv_profile_cars.setAdapter(language_adapter);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        btn_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_ride_amt.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "Please enter budget", Toast.LENGTH_SHORT).show();
                }else if(pref.getcartype().equals("")){
                    Toast.makeText(getActivity(), "Please select car type", Toast.LENGTH_SHORT).show();
                }else {
                    createride();
                }

            }
        });


        img_bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.cab_fragment_to_notification_fragment);
            }
        });

        img_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.cab_fragment_to_order_fragment);
            }
        });

        ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.cab_fragment_to_chat_fragment);
            }
        });

        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.cab_fragment_to_pasenger_profile);
            }
        });

    }

    private void createride() {

        String rawdata = getJson();
        TypedInput in = null;
        try {
            in = new TypedByteArray("application/json", rawdata.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (Utils.isNetworkAvailable(getActivity())) {
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.create_ride(
                        "Token "+ pref.getauthtoke(),
                        in,
                        new Callback<Create_Ride_Model>() {
                            @Override
                            public void success(Create_Ride_Model otp_valid_model, Response response) {

                                Utils.hideProgress1();

                                String ride_id = otp_valid_model.getId();
                                String budget = otp_valid_model.getBudget();
                                Bundle args = new Bundle();
                                args.putString("ride_id", ride_id);
                                args.putString("budget", budget);
                                pref.setcartype("");
                             //   navController.navigate(R.id.cab_fragment_to_result_fragment ,args);
                                navController.navigate(R.id.cab_fragment_to_order_fragment ,args);

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Log.e("TAG" , "CHECK_ERROR" + error);
                                Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }
                        });
        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }



        /*if (Utils.isNetworkAvailable(getActivity())) {
            Utils.showProgress1("Please wait", getActivity());
            CaberWSapi api = APIService.createService(CaberWSapi.class);
            api.create_ride(
                    "Token "+ pref.getauthtoke(),
                    pref.getcartype(),
                    et_ride_amt.getText().toString(),
                    pickup_name,
                    pickup_name,
                    pickup_latitude,
                    pickup_longitude,
                    drop_name,
                    drop_name,
                    drop_latitude,
                    drop_longitude,
                    cab,
                    new Callback<Create_Ride_Model>() {
                        @Override
                        public void success(Create_Ride_Model otp_valid_model, Response response) {
                            Utils.hideProgress1();

                            String ride_id = otp_valid_model.getId();
                            String budget = otp_valid_model.getBudget();
                            Bundle args = new Bundle();
                            args.putString("ride_id", ride_id);
                            args.putString("budget", budget);
                            pref.setcartype("");
                            navController.navigate(R.id.cab_fragment_to_result_fragment ,args);

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                            Utils.hideProgress1();
                        }
                    });


        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }*/

    }

    public String getJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("car_type",  pref.getcartype());
            obj.put("budget",et_ride_amt.getText().toString());
            obj.put("pickup_name", pickup_name);
            obj.put("pickup_address",pickup_name);
            obj.put("pickup_latitude",pickup_latitude);
            obj.put("pickup_longitude",pickup_longitude);
            obj.put("drop_name",drop_name);
            obj.put("drop_address",drop_name);
            obj.put("drop_latitude",drop_latitude);
            obj.put("drop_longitude",drop_longitude);
            obj.put("ride_type",cab);

        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return obj.toString();
    }



}
