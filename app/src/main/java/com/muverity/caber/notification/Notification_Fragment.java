package com.muverity.caber.notification;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.adapter.Notification_Adapter;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Notification_Fragment extends Fragment {

    ImageView iv_back;
    Notification_Adapter language_adapter;
    RecyclerView rv_notification ;
    ArrayList<Notification_Model> list = new ArrayList<>();
    PreferenceUtils pref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.driver_notification_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        iv_back = view.findViewById(R.id.iv_back);
        rv_notification = (RecyclerView)view. findViewById(R.id.rv_notification);
        pref = new PreferenceUtils(getActivity());

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        rv_notification.setHasFixedSize(true);
        rv_notification.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.VERTICAL, false));

    }
    @Override
    public void onResume() {
        super.onResume();

        passenger_notification();

    }

    private void passenger_notification() {
        try {
            if (Utils.isNetworkAvailable(getActivity())) {
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.passenger_notification_list(
                        "Token "+ pref.getauthtoke(),
                        new Callback<ArrayList<Notification_Model>>() {
                            @Override
                            public void success(ArrayList<Notification_Model> modelLeaves, Response response) {

                                Utils.hideProgress1();
                                list = modelLeaves;
                                language_adapter = new Notification_Adapter(getActivity(), list);
                                rv_notification.setAdapter(language_adapter);
                                language_adapter.notifyDataSetChanged();

                            }
                            @Override
                            public void failure(RetrofitError error) {

                                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                                Log.e("TAG" , "TEST_ERROR" +error);
                                Utils.hideProgress1();
                            }

                        });
            } else {
                Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
