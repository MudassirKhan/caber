package com.muverity.caber.notification;

import com.muverity.caber.model.DriverNotification_Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Notification_Model implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("notification_text")
    @Expose
    private String notificationText;
    @SerializedName("passenger_details")
    @Expose
    private Notification_Model.PassengerDetails passengerDetails;
    @SerializedName("ride_details")
    @Expose
    private Notification_Model.RideDetails rideDetails;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("driver_details")
    @Expose
    private Notification_Model.DriverDetails driverDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public Notification_Model.PassengerDetails getPassengerDetails() {
        return passengerDetails;
    }

    public void setPassengerDetails(Notification_Model.PassengerDetails passengerDetails) {
        this.passengerDetails = passengerDetails;
    }

    public Notification_Model.RideDetails getRideDetails() {
        return rideDetails;
    }

    public void setRideDetails(Notification_Model.RideDetails rideDetails) {
        this.rideDetails = rideDetails;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Notification_Model.DriverDetails getDriverDetails() {
        return driverDetails;
    }

    public void setDriverDetails(Notification_Model.DriverDetails driverDetails) {
        this.driverDetails = driverDetails;
    }

    public class PassengerDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private Object lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("country_code")
        @Expose
        private String countryCode;

        @SerializedName("profile_pic")
        @Expose
        private String profilePic;

        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("auth_token")
        @Expose
        private String authToken;
        @SerializedName("role")
        @Expose
        private Integer role;
        @SerializedName("total_rides")
        @Expose
        private Integer totalRides;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public Object getLastName() {
            return lastName;
        }

        public void setLastName(Object lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public Integer getRole() {
            return role;
        }

        public void setRole(Integer role) {
            this.role = role;
        }

        public Integer getTotalRides() {
            return totalRides;
        }

        public void setTotalRides(Integer totalRides) {
            this.totalRides = totalRides;
        }

    }

    public class RideDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("passenger")
        @Expose
        private Integer passenger;
        @SerializedName("car_type")
        @Expose
        private String carType;
        @SerializedName("budget")
        @Expose
        private String budget;
        @SerializedName("pickup_name")
        @Expose
        private String pickupName;
        @SerializedName("pickup_address")
        @Expose
        private String pickupAddress;
        @SerializedName("pickup_latitude")
        @Expose
        private String pickupLatitude;
        @SerializedName("pickup_longitude")
        @Expose
        private String pickupLongitude;
        @SerializedName("drop_name")
        @Expose
        private String dropName;
        @SerializedName("drop_address")
        @Expose
        private String dropAddress;
        @SerializedName("drop_latitude")
        @Expose
        private String dropLatitude;
        @SerializedName("drop_longitude")
        @Expose
        private String dropLongitude;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("ride_type")
        @Expose
        private String rideType;
        @SerializedName("status")
        @Expose
        private String status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPassenger() {
            return passenger;
        }

        public void setPassenger(Integer passenger) {
            this.passenger = passenger;
        }

        public String getCarType() {
            return carType;
        }

        public void setCarType(String carType) {
            this.carType = carType;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getPickupName() {
            return pickupName;
        }

        public void setPickupName(String pickupName) {
            this.pickupName = pickupName;
        }

        public String getPickupAddress() {
            return pickupAddress;
        }

        public void setPickupAddress(String pickupAddress) {
            this.pickupAddress = pickupAddress;
        }

        public String getPickupLatitude() {
            return pickupLatitude;
        }

        public void setPickupLatitude(String pickupLatitude) {
            this.pickupLatitude = pickupLatitude;
        }

        public String getPickupLongitude() {
            return pickupLongitude;
        }

        public void setPickupLongitude(String pickupLongitude) {
            this.pickupLongitude = pickupLongitude;
        }

        public String getDropName() {
            return dropName;
        }

        public void setDropName(String dropName) {
            this.dropName = dropName;
        }

        public String getDropAddress() {
            return dropAddress;
        }

        public void setDropAddress(String dropAddress) {
            this.dropAddress = dropAddress;
        }

        public String getDropLatitude() {
            return dropLatitude;
        }

        public void setDropLatitude(String dropLatitude) {
            this.dropLatitude = dropLatitude;
        }

        public String getDropLongitude() {
            return dropLongitude;
        }

        public void setDropLongitude(String dropLongitude) {
            this.dropLongitude = dropLongitude;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getRideType() {
            return rideType;
        }

        public void setRideType(String rideType) {
            this.rideType = rideType;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }

    public class DriverDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("country_code")
        @Expose
        private String countryCode;
        @SerializedName("car_model")
        @Expose
        private String carModel;
        @SerializedName("driver_licence")
        @Expose
        private String driverLicence;
        @SerializedName("car_ownership_doc")
        @Expose
        private String carOwnershipDoc;
        @SerializedName("profile_pic")
        @Expose
        private String profilePic;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("auth_token")
        @Expose
        private String authToken;
        @SerializedName("car_pic")
        @Expose
        private List<CarPic> carPic = null;
        @SerializedName("role")
        @Expose
        private Integer role;
        @SerializedName("car_type")
        @Expose
        private String carType;
        @SerializedName("total_rides")
        @Expose
        private Integer totalRides;
        @SerializedName("total_rating")
        @Expose
        private Double totalRating;
        @SerializedName("plan_details")
        @Expose
        private PlanDetails planDetails;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getCarModel() {
            return carModel;
        }

        public void setCarModel(String carModel) {
            this.carModel = carModel;
        }

        public String getDriverLicence() {
            return driverLicence;
        }

        public void setDriverLicence(String driverLicence) {
            this.driverLicence = driverLicence;
        }

        public String getCarOwnershipDoc() {
            return carOwnershipDoc;
        }

        public void setCarOwnershipDoc(String carOwnershipDoc) {
            this.carOwnershipDoc = carOwnershipDoc;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public List<CarPic> getCarPic() {
            return carPic;
        }

        public void setCarPic(List<CarPic> carPic) {
            this.carPic = carPic;
        }

        public Integer getRole() {
            return role;
        }

        public void setRole(Integer role) {
            this.role = role;
        }

        public String getCarType() {
            return carType;
        }

        public void setCarType(String carType) {
            this.carType = carType;
        }

        public Integer getTotalRides() {
            return totalRides;
        }

        public void setTotalRides(Integer totalRides) {
            this.totalRides = totalRides;
        }

        public Double getTotalRating() {
            return totalRating;
        }

        public void setTotalRating(Double totalRating) {
            this.totalRating = totalRating;
        }

        public PlanDetails getPlanDetails() {
            return planDetails;
        }

        public void setPlanDetails(PlanDetails planDetails) {
            this.planDetails = planDetails;
        }



    }

    public class PlanDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("duration")
        @Expose
        private Integer duration;
        @SerializedName("cost")
        @Expose
        private String cost;
        @SerializedName("descriptions")
        @Expose
        private List<String> descriptions = null;
        @SerializedName("plan_expiry_date")
        @Expose
        private String planExpiryDate;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getDuration() {
            return duration;
        }

        public void setDuration(Integer duration) {
            this.duration = duration;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public List<String> getDescriptions() {
            return descriptions;
        }

        public void setDescriptions(List<String> descriptions) {
            this.descriptions = descriptions;
        }

        public String getPlanExpiryDate() {
            return planExpiryDate;
        }

        public void setPlanExpiryDate(String planExpiryDate) {
            this.planExpiryDate = planExpiryDate;
        }

    }

    public class CarPic {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("image")
        @Expose
        private String image;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }


}
