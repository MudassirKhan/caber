package com.muverity.caber.home;

import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.muverity.caber.AutonPlace_Activity;
import com.muverity.caber.R;
import com.muverity.caber.api.PreferenceUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class Home_Fragment extends Fragment implements OnMapReadyCallback , LocationListener {

    private GoogleMap mMap;
    private MapView mMapView;
    Marker mCurrLocationMarker;
    Location mLastLocation;

    Button btn_delievry , btn_cab;

    ImageView img_notification , img_order , iv_back ;
    LinearLayout ll_chat , ll_profile;

    EditText et_leaving , et_going;
    LinearLayout ll_img_leaving , ll_img_going ;

    String laddress = "";
    String lcity = "";
    String lcountry = "";
    double llatitude ;
    double llongitutde ;
    String lpostalcode ;

    String gaddress = "";
    String gcity = "";
    String gcountry = "";
    double glatitude ;
    double glongitutde ;
    String gpostalcode ;
    String dropname ;
    String pickname ;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    int AUTOCOMPLETE_REQUEST_CODE2 = 2;

    ImageView img_main ;
    TextView tv_name;
    PreferenceUtils pref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        mMapView = view.findViewById(R.id.mapView);
        btn_delievry = view.findViewById(R.id.btn_delievry);
        btn_cab = view.findViewById(R.id.btn_cab);
        iv_back = view.findViewById(R.id.iv_back);

        img_notification = view.findViewById(R.id.img_notification);
        img_order = view.findViewById(R.id.img_order);
        ll_chat = view.findViewById(R.id.ll_chat);
        ll_profile = view.findViewById(R.id.ll_profile);

        et_leaving = view.findViewById(R.id.et_leaving);
        et_going = view.findViewById(R.id.et_going);
        ll_img_leaving = view.findViewById(R.id.ll_img_leaving);
        ll_img_going = view.findViewById(R.id.ll_img_going);

        img_main = view.findViewById(R.id.img_main);
        tv_name = view.findViewById(R.id.tv_name);
        pref = new PreferenceUtils(getActivity());


        if (pref.getprofile_pic() != null) {
            if (!pref.getprofile_pic().equals("")) {
                Picasso.with(getActivity()).load(pref.getprofile_pic()).placeholder(R.drawable.default_profile).into(img_main);
            }
        }

        if (pref.getfirstname() != null) {
            if (!pref.getfirstname().equals("")) {
                tv_name.setText("Hey"+" "+pref.getfirstname());
                //  et_name.setText(pref.getfirstname() + " "+ pref.getlastname());
            }
        }

        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
        mMapView.onResume(); // needed to get the map to display immediately

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        btn_delievry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_leaving.getText().toString().equals("") || et_leaving.length() == 0){

                    Toast.makeText(getActivity(), "Please select leaving location", Toast.LENGTH_SHORT).show();

                }else if(et_going.getText().toString().equals("") || et_going.length() ==0){

                    Toast.makeText(getActivity(), "Please selectgoing location", Toast.LENGTH_SHORT).show();

                }else {

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btn_cab.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_state_back));
                            btn_cab.setTextColor(Color.parseColor("#00008B"));
                            btn_delievry.setBackgroundDrawable(getResources().getDrawable(R.drawable.cabbutton_state_back));
                            btn_delievry.setTextColor(Color.parseColor("#FFFFFF"));

                            Bundle args = new Bundle();
                            args.putString("pickup_name", lcity);
                            args.putString("pickup_address", laddress);
                            args.putString("pickup_latitude", String.valueOf(llatitude));
                            args.putString("pickup_longitude", String.valueOf(llongitutde));
                            args.putString("drop_name", gcity);
                            args.putString("drop_address", gaddress);
                            args.putString("drop_latitude", String.valueOf(glatitude));
                            args.putString("drop_longitude", String.valueOf(glongitutde));
                            args.putString("cab", "Delivery");
                            navController.navigate(R.id.home_fragment_to_cab_fragment , args);

                        }
                    }, 100);

                }

            }
        });

        btn_cab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_leaving.getText().toString().equals("") || et_leaving.length() == 0){

                    Toast.makeText(getActivity(), "Please select leaving location", Toast.LENGTH_SHORT).show();

                }else if(et_going.getText().toString().equals("") || et_going.length() ==0){

                    Toast.makeText(getActivity(), "Please selectgoing location", Toast.LENGTH_SHORT).show();

                }else {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btn_delievry.setBackgroundDrawable(getResources().getDrawable(R.drawable.cabbutton_state_back));
                            btn_delievry.setTextColor(Color.parseColor("#FFFFFF"));

                            btn_cab.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_state_back));
                            btn_cab.setTextColor(Color.parseColor("#00008B"));
                            Bundle args = new Bundle();
                            args.putString("pickup_name", lcity);
                            args.putString("pickup_address", laddress);
                            args.putString("pickup_latitude", String.valueOf(llatitude));
                            args.putString("pickup_longitude", String.valueOf(llongitutde));
                            args.putString("drop_name", gcity);
                            args.putString("drop_address", gaddress);
                            args.putString("drop_latitude", String.valueOf(glatitude));
                            args.putString("drop_longitude", String.valueOf(glongitutde));
                            args.putString("cab", "Cab");
                            navController.navigate(R.id.home_fragment_to_cab_fragment , args);

                        }

                    }, 0);

                }

            }
        });


        img_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.home_fragment_to_notification_fragment);
            }
        });

        img_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.home_fragment_to_order_fragment);
            }
        });

        ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.home_fragment_to_chat_fragment);
            }
        });

        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.home_fragment_to_pasenger_profile);
            }
        });

        et_leaving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AutonPlace_Activity.class);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);

            }
        });

        et_going.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AutonPlace_Activity.class);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE2);

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        this.mMap = googleMap;
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        addMarker(22.5726, 88.3639);

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = this.mMap.addMarker(markerOptions);

        //move map camera
        this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));


    }

    private void addMarker(double lat, double lng) {
        this.mMap.clear();
        this.mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10));
    }


   /* public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {

            if (data != null) {
                if (requestCode == 1) {
                    Log.e("TAG", "Location Found Lat ==== " + data.getStringExtra("lat"));
                    Log.e("TAG", "Location Found Lng ==== " + data.getStringExtra("lng"));

                    double latit = Double.parseDouble(data.getStringExtra("lat"));
                    double longit = Double.parseDouble(data.getStringExtra("lng"));

                    try {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        addresses = geocoder.getFromLocation(latit, longit, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        laddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        lcity = addresses.get(0).getLocality();
                        lpostalcode = addresses.get(0).getPostalCode();
                        lcountry = addresses.get(0).getCountryName();
                        llatitude = addresses.get(0).getLatitude();
                        llongitutde = addresses.get(0).getLongitude();
                        et_leaving.setText(laddress);

                    } catch (Exception e) {

                    }
                }
            }

        }else {

            if (data != null) {
                if (requestCode == 2) {
                    Log.e("TAG", "Location Found Lat ==== " + data.getStringExtra("lat"));
                    Log.e("TAG", "Location Found Lng ==== " + data.getStringExtra("lng"));

                    double latit = Double.parseDouble(data.getStringExtra("lat"));
                    double longit = Double.parseDouble(data.getStringExtra("lng"));

                    try {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        addresses = geocoder.getFromLocation(latit, longit, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        gaddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        gcity = addresses.get(0).getLocality();
                        gpostalcode = addresses.get(0).getPostalCode();
                        gcountry = addresses.get(0).getCountryName();
                        glatitude = addresses.get(0).getLatitude();
                        glongitutde = addresses.get(0).getLongitude();

                        et_going.setText(gaddress);


                    } catch (Exception e) {

                    }
                }
            }

        }
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                String address = data.getStringExtra("address");
                try {
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    addresses = geocoder.getFromLocationName(address, 1);
                    laddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    lcity = addresses.get(0).getLocality();
                    lpostalcode = addresses.get(0).getPostalCode();
                    lcountry = addresses.get(0).getCountryName();
                    llatitude = addresses.get(0).getLatitude();
                    llongitutde = addresses.get(0).getLongitude();
                    pickname = addresses.get(0).getSubLocality();
                    et_leaving.setText(laddress);

                    Log.e("TAG","Address_Pname"+laddress);
                    Log.e("TAG","Address_Pname"+lcity);
                    Log.e("TAG","Address_Pname"+pickname);
                    Log.e("TAG","Address_Pname"+addresses.get(0).getSubAdminArea());
                    Log.e("TAG","Address_Pname"+addresses.get(0).getAdminArea());

                    Log.e("TAG" , "NAMES_subArea" +    addresses.get(0).getSubLocality());




                } catch (Exception e) {

                }


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("TAG", status.getStatusMessage());
            } else if (resultCode == 0) {
                // The user canceled the operation.
            }


        }else {
            if (resultCode == RESULT_OK) {

                String address = data.getStringExtra("address");

                try {
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    addresses = geocoder.getFromLocationName(address, 1);
                    gaddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    gcity = addresses.get(0).getLocality();
                    gpostalcode = addresses.get(0).getPostalCode();
                    gcountry = addresses.get(0).getCountryName();
                    glatitude = addresses.get(0).getLatitude();
                    glongitutde = addresses.get(0).getLongitude();
                    String dropname = addresses.get(0).getSubLocality();

                    et_going.setText(gaddress);




                } catch (Exception e) {

                }


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("TAG", status.getStatusMessage());
            } else if (resultCode == 0) {
                // The user canceled the operation.
            }

        }
    }

}




















































  /*  SupportMapFragment mapFragment;
    GoogleMap mMap;
    private PlaceAutocompleteFragment autocompleteFragment;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private GoogleApiClient mGoogleApiClient;
    Button btnSubmit;
    double lat = 0.0, lng = 0.0;
    Intent intent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        // mapFragment = (SupportMapFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        mapFragment.getMapAsync(this);

        LocationManager locationManager = (LocationManager)getActivity().getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            intent = new Intent(getActivity().getApplicationContext(), AccurateLocationService.class);
            AccurateLocationService.enqueueWork(getActivity().getApplicationContext(), intent);
        } else {
            showGPSDisabledAlertToUser();
        }

        mGoogleApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        autocompleteFragment = (PlaceAutocompleteFragment) getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();
        autocompleteFragment.setFilter(typeFilter);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                LatLng queriedLocation = place.getLatLng();
                lat = queriedLocation.latitude;
                lng = queriedLocation.longitude;

                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 12.0f));
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("TAG", "An error occurred: " + status);
            }
        });

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        mMap = googleMap;
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragStart..." + arg0.getPosition().latitude + "..." + arg0.getPosition().longitude);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onMarkerDragEnd(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragEnd..." + arg0.getPosition().latitude + "..." + arg0.getPosition().longitude);
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
            }

            @Override
            public void onMarkerDrag(Marker arg0) {
                // TODO Auto-generated method stub
                Log.i("System out", "onMarkerDrag...");
            }
        });

        mMap.setMyLocationEnabled(true);
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat[0], lng[0]), 10));
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        intent = new Intent(getActivity(), AccurateLocationService.class);
        AccurateLocationService.enqueueWork(getActivity(), intent);
        getActivity(). startService(intent);
        getActivity(). registerReceiver(broadcastReceiver, new IntentFilter(AccurateLocationService.BROADCAST_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity(). unregisterReceiver(broadcastReceiver);
        getActivity().stopService(intent);
    }

private class GeocoderHandler extends Handler {
    @Override
    public void handleMessage(Message message) {
        String locationAddress;
        switch (message.what) {
            case 1:
                Bundle bundle = message.getData();
                locationAddress = bundle.getString("address");
                break;
            default:
                locationAddress = null;
        }
        lat = Double.parseDouble(locationAddress.split(":")[0]);
        lng = Double.parseDouble(locationAddress.split(":")[1]);
        LatLng sydney = new LatLng(lat,
                lng);
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(sydney).title(""));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12.0f));
    }
}

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (lat == 0.0) {
                updateUI(intent);
            }
        }
    };

    private void updateUI(Intent intent) {
        String strlat = intent.getStringExtra("lat");
        String strlng = intent.getStringExtra("lng");

        lat = Double.parseDouble(strlat);
        lng = Double.parseDouble(strlng);
        mMap.clear();
        LatLng sydney = new LatLng(Double.parseDouble(strlat), Double.parseDouble(strlng));
        mMap.addMarker(new MarkerOptions().position(sydney).title(""));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12.0f));

    }*/