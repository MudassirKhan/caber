package com.muverity.caber.caber;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.model.Accetpt_Ride_Model;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class ImageAdapter  extends RecyclerView.Adapter<ImageAdapter.ViewHolder>{

    ArrayList<Accetpt_Ride_Model.CarPic>  list;
    private Context mCtx;
    public ImageAdapter(Context mCtx, ArrayList<Accetpt_Ride_Model.CarPic>  list) {
        this.mCtx = mCtx;
        this.list = list;
    }

    @NonNull
    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.row_images, parent, false);
        ImageAdapter.ViewHolder viewHolder = new ImageAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageAdapter.ViewHolder holder, int position) {

        if (list.get(position)!= null) {
            if (!list.get(position).equals("")) {
                Picasso.with(mCtx)
                        .load(list.get(position).getImage())
                        .placeholder(R.drawable.ic_car)
                        .into(holder.img_car1);
            }
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_car1;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_car1 = itemView.findViewById(R.id.img_car1);

        }
    }
}
