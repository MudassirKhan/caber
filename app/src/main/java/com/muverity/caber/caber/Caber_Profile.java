package com.muverity.caber.caber;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.model.Accetpt_Ride_Model;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Caber_Profile extends Fragment {

    ImageView iv_back , img_main;
    TextView tv_name , tv_total_ride;
    RecyclerView rv_profile_cars;
    RatingBar ratingBar;
    ImageAdapter imageAdapter;
    int driver_id ,total_rides ,number ,ride_id,passenger_id;
    String total_rating;
    String name ,budget , pick , drop ,pick_lat,pick_long,drop_lat,drop_lng ,ride_type,status,profile;
    ArrayList<Accetpt_Ride_Model.CarPic> list ;
    LinearLayout ll_chat ,ll_profile ;
    ImageView img_order;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.caber_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        iv_back = view.findViewById(R.id.iv_back);
        img_main = view.findViewById(R.id.img_main);
        tv_total_ride = view.findViewById(R.id.tv_total_ride);
        tv_name = view.findViewById(R.id.tv_name);
        rv_profile_cars = view.findViewById(R.id.rv_profile_cars);
        ratingBar = view.findViewById(R.id.ratingBar);
        ll_chat = view.findViewById(R.id.ll_chat);
        ll_profile = view.findViewById(R.id.ll_profile);
        img_order = view.findViewById(R.id.img_order);

        driver_id  = getArguments().getInt("driver_id");
        total_rides  = getArguments().getInt("total_rides");
        total_rating  = getArguments().getString("total_rating");
        name  = getArguments().getString("name");
        list= (ArrayList<Accetpt_Ride_Model.CarPic>)getArguments().getSerializable("list");
        profile  = getArguments().getString("profile");

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.caber_profile_to_chat_fragment);
            }
        });

        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                navController.navigate(R.id.caber_profile_to_pasenger_profile);
            }
        });

        img_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.caber_profile_to_order_fragment);
            }
        });


        if (profile != null) {
            if (!profile.equals("")) {
                Picasso.with(getActivity()).load(profile).placeholder(R.drawable.default_profile).into(img_main);
            }
        }

        if (name!= null) {
            if (!name.equals("")) {
                tv_name.setText(name );
            }
        }

        tv_total_ride.setText( String.valueOf(total_rides) + "+ times");

        if (total_rating != null) {
            if (!total_rating.equals("")) {
                double rating  = Double.parseDouble(String.valueOf(total_rating));
                //double rating  = Double.parseDouble(String.valueOf(2.5));
                ratingBar.setRating((float) rating);
            }
        }

      /*  rv_profile_cars.setHasFixedSize(true);
        rv_profile_cars.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.VERTICAL, false));
        imageAdapter = new ImageAdapter(getActivity(), list);
        rv_profile_cars.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();*/

        GridLayoutManager gLayoutManager = new GridLayoutManager(getActivity(), 2); // (Context context, int spanCount)
        rv_profile_cars.setLayoutManager(gLayoutManager);
        rv_profile_cars.setItemAnimator(new DefaultItemAnimator());
        imageAdapter = new ImageAdapter(getActivity(), list);
        rv_profile_cars.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();

    }
}
