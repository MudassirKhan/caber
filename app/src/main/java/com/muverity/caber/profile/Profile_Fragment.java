package com.muverity.caber.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.muverity.caber.MyApplication;
import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.AppConst;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.FilePath;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.DriverUpdateProfile_Model;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static android.content.Context.MODE_PRIVATE;

public class Profile_Fragment extends Fragment {

   /* Profile_Model[] cars_images = new Profile_Model[]{
            new Profile_Model(  R.drawable.ic_profile_car1),
            new Profile_Model(  R.drawable.ic_profile_car2),
    };*/

    ImageView img_upload, img_editname, img_editnamesave, img_car1_save, img_car2_save, img_bell , img_profile , img_car1 , img_car2;
    TextView tv_edit, tv_done, tv_logout ,tv_totalride;
    private int SELECT_LICENS = 1;
    private int SELECT_OWNERSHIP = 2;
    private int SELECT_PHOTO = 3;
    String token;
    private Uri filePath;
    private Uri filePath2;
    EditText et_name;
    private Uri filePath3;
    PreferenceUtils pref;
    String fileType = AppConst.FILE_TYPE_TEXT, fileExtention = "NULL", fileURL = "NULL", fileDeviceURL = "NULL", messageText = "NULL";

    LinearLayout ll_chat, ll_profile;
    ImageView img_order, iv_back;
    RatingBar ratingBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
        pref = new PreferenceUtils(getActivity());
        img_upload = view.findViewById(R.id.img_upload);
        img_editname = view.findViewById(R.id.img_editname);
        img_editnamesave = view.findViewById(R.id.img_editnamesave);
        img_car1_save = view.findViewById(R.id.img_car1_save);
        img_car2_save = view.findViewById(R.id.img_car2_save);
        tv_edit = view.findViewById(R.id.tv_edit);
        tv_done = view.findViewById(R.id.tv_done);
        tv_logout = view.findViewById(R.id.tv_logout);
        img_bell = view.findViewById(R.id.img_bell);
        et_name = view.findViewById(R.id.et_name);
        ll_chat = view.findViewById(R.id.ll_chat);
        img_order = view.findViewById(R.id.img_order);
        iv_back = view.findViewById(R.id.iv_back);
        img_profile = view.findViewById(R.id.img_profile);
        tv_totalride = view.findViewById(R.id.tv_totalride);
        ratingBar = view.findViewById(R.id.ratingBar);
        img_car1 = view.findViewById(R.id.img_car1);
        img_car2 = view.findViewById(R.id.img_car2);

        et_name.setEnabled(false);

        if (pref.getprofile_pic() != null) {
            if (!pref.getprofile_pic().equals("")) {
                Picasso.with(getActivity()).load(pref.getprofile_pic())
                        .placeholder(R.drawable.default_profile)
                        .into(img_profile);
            }
        }

        if (pref.getfirstname() != null) {
            if (!pref.getfirstname().equals("")) {
                et_name.setText(pref.getfirstname());
            }
        }

        tv_totalride.setText(String.valueOf(pref.gettotal_rides()));

        if (pref.getsettotal_rating() != null) {
            if (!pref.getsettotal_rating().equals("")) {
                double rating  = Double.parseDouble(pref.getsettotal_rating());
                //double rating  = Double.parseDouble(String.valueOf(2.5));
                ratingBar.setRating((float) rating);
            }
        }
        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("USER",MODE_PRIVATE);
        String json_cars = sharedPreferences.getString("Set", "");

        if(! json_cars.equals("")){

            JSONArray arr = null;
            try {
                arr = new JSONArray(json_cars);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            List<String> list = new ArrayList<String>();
            for(int i = 0; i < arr.length(); i++){
                try {
                    list.add(arr.getJSONObject(i).getString("image"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (list.get(0)  != null) {
                if (!list.get(0) .equals("")) {
                    Picasso.with(getActivity()).load(list.get(0)).placeholder(R.drawable.ic_car).into(img_car1);
                }
            }

            if (list.get(1)  != null) {
                if (!list.get(1) .equals("")) {
                    Picasso.with(getActivity()).load(list.get(1)).placeholder(R.drawable.ic_car).into(img_car2);
                }
            }
            Log.e("TAG" , "TEST_DATA" + json_cars) ;
        }


       /* RecyclerView rv_profile_cars = (RecyclerView) view.findViewById(R.id.rv_profile_cars);
        GridLayoutManager gLayoutManager = new GridLayoutManager(getActivity(), 2); // (Context context, int spanCount)
        rv_profile_cars.setLayoutManager(gLayoutManager);
        rv_profile_cars.setItemAnimator(new DefaultItemAnimator());
        Profile_adapter gridview_adapter = new Profile_adapter(cars_images);
        rv_profile_cars.setAdapter(gridview_adapter);
*/

        ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.profile_fragment_to_chat_fragment);
            }
        });

        img_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.profile_fragment_to_driver_order_fragment);
            }
        });

        img_bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.profile_fragment_to_driver_notification);
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pref.setlogin("0");
                pref.setfirstname("");
                pref.setlastname("");
                pref.setemail("");
                pref.setrole("");
                pref.setcar_model("");
                pref.setcar_ownership_doc("");
                pref.setcountry_code("");
                pref.setdriver_licence("");
                pref.setprofile_pic("");
                pref.setauthtoke("");
                pref.setuserid(0);
                pref.setimagepath("");
                pref.settotal_rating(0.0);
                pref.setprofilestatus("");
                pref.setcarstatus("");
                pref.settransid(0);
                navController.navigate(R.id.profile_fragment_to_splash3_fragment);

            }
        });


        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_upload.setVisibility(View.VISIBLE);
                img_editname.setVisibility(View.VISIBLE);
                img_car1_save.setVisibility(View.VISIBLE);
                img_car2_save.setVisibility(View.VISIBLE);
                tv_done.setVisibility(View.VISIBLE);
                tv_edit.setVisibility(View.GONE);

            }
        });

        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_upload.setVisibility(View.GONE);
                img_editname.setVisibility(View.GONE);
                img_car1_save.setVisibility(View.GONE);
                img_car2_save.setVisibility(View.GONE);
                tv_done.setVisibility(View.GONE);
                tv_edit.setVisibility(View.VISIBLE);

                UpdateDriverProfile();

            }
        });

        img_editname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_name.setEnabled(true);
                et_name.setFocusable(true);
                et_name.requestFocus();
                et_name.setCursorVisible(true);
                img_editnamesave.setVisibility(View.VISIBLE);
                img_editname.setVisibility(View.GONE);
            }
        });

        img_editnamesave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.setpassengername(et_name.getText().toString());
                img_editnamesave.setVisibility(View.GONE);
                img_editname.setVisibility(View.VISIBLE);
                et_name.setEnabled(true);
                et_name.clearFocus();
                et_name.requestFocus();
                et_name.setCursorVisible(false);
                hideSoftKeyboard(v);

            }
        });

        img_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_PHOTO);
            }
        });

        img_car1_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_LICENS);
            }
        });

        img_car2_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_OWNERSHIP);
            }
        });


    }

    private void UpdateDriverProfile() {

        TypedFile uploadpic1 = null;
        if (getFilePath() != null) {
            uploadpic1 = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), getFilePath())));
        }

        TypedFile uploadpic2 = null;
        if (getFilePath2() != null) {
            uploadpic2 = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), getFilePath2())));
        }

        TypedFile uploadprofile = null;
        if (getFilePath3() != null) {

            pref.setimagepath(FilePath.getPath(getActivity(), getFilePath3()));
            uploadprofile = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), getFilePath3())));

        }else {
            uploadpic1 = new TypedFile("multipart/form-data",  new File(pref.getimagepath()));
        }

        if (et_name.getText().toString().equals("")) {

            Toast.makeText(MyApplication.getAppContext(), "Please enter name", Toast.LENGTH_SHORT).show();

        } else

            if (Utils.isNetworkAvailable(getActivity())) {
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.driver_update_profile(
                        pref.getuserid(),
                        "Token "+ pref.getauthtoke(),
                        et_name.getText().toString(),
                        et_name.getText().toString(),
                        pref.getemail(),
                        "android",
                        pref.gettoken(),
                        uploadprofile,
                        uploadpic1,
                        uploadpic2,
                        new Callback<DriverUpdateProfile_Model>() {
                            @Override
                            public void success(DriverUpdateProfile_Model otp_valid_model, Response response) {
                                Utils.hideProgress1();


                                pref.setfirstname(otp_valid_model.getFirstName());
                                pref.setemail(otp_valid_model.getEmail());
                                pref.setprofile_pic(otp_valid_model.getProfilePic());
                                pref.setrole(String.valueOf(otp_valid_model.getRole()));

                                Gson gson = new Gson();
                                final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("USER",MODE_PRIVATE);
                                String json = gson.toJson((ArrayList) otp_valid_model.getCarPic());
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("Set",json );
                                editor.apply();

                                Toast.makeText(getActivity(), "Profile successfully updated...", Toast.LENGTH_SHORT).show();

                            }
                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                            }
                        });



            } else {
                Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
            }






}

    private void hideSoftKeyboard(View v){
        InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }

    public Uri getFilePath2() {
        return filePath2;
    }

    public void setFilePath2(Uri filePath2) {
        this.filePath2 = filePath2;
    }

    public Uri getFilePath3() {
        return filePath3;
    }

    public void setFilePath3(Uri filePath3) {
        this.filePath3 = filePath3;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_LICENS) {

                setFilePath(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());
                setFilePath(uriFromPath);

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    img_car1_save.setVisibility(View.GONE);
                    img_car1.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }

            }else if (requestCode == SELECT_OWNERSHIP){

                setFilePath2(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());
                setFilePath2(uriFromPath);

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    img_car2_save.setVisibility(View.GONE);
                    img_car2.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }



            }else {

                setFilePath3(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());
                setFilePath3(uriFromPath);

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    img_profile.setImageBitmap(selectedImage);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }

        }

    }



}
