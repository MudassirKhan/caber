package com.muverity.caber.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.muverity.caber.MyApplication;
import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.AppConst;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.FilePath;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.Passenger_update_Model;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class Pasender_Profile_Fragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private GoogleMap googleMap;
    SupportMapFragment mapFragment;
    Marker mCurrLocationMarker;
    Location mLastLocation;

    ImageView img_notification , img_order , iv_back;
    LinearLayout ll_chat , ll_profile;
    ImageView img_upload , img_editname , img_editnumber;
    private int SELECT_PHOTO = 1;
    private Uri filePath;
    EditText et_name , et_phone;
    PreferenceUtils pref;
    ImageView img_profile;
    String fileType = AppConst.FILE_TYPE_TEXT, fileExtention = "NULL", fileURL = "NULL", fileDeviceURL = "NULL", messageText = "NULL";
    ImageView img_editnumbersave , img_editnamesave;
    TextView tv_total_ride , tv_logout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pasenger_profile_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        changeStatusBarColor(getResources().getColor(R.color.white));
        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
        img_notification = view.findViewById(R.id.img_notification);
        img_order = view.findViewById(R.id.img_order);
        ll_chat = view.findViewById(R.id.ll_chat);
        ll_profile = view.findViewById(R.id.ll_profile);
        img_editname = view.findViewById(R.id.img_editname);
        img_upload = view.findViewById(R.id.img_upload);
        img_editnumber = view.findViewById(R.id.img_editnumber);
        pref = new PreferenceUtils(getActivity());
        et_name = view.findViewById(R.id.et_name);
        et_phone = view.findViewById(R.id.et_phone);
        img_profile = view.findViewById(R.id.img_profile);
        img_editnumbersave = view.findViewById(R.id.img_editnumbersave);
        img_editnamesave = view.findViewById(R.id.img_editnamesave);
        iv_back = view.findViewById(R.id.iv_back);
        tv_total_ride = view.findViewById(R.id.tv_total_ride);
        tv_logout = view.findViewById(R.id.tv_logout);

        et_name.setEnabled(false);
        et_phone.setEnabled(false);

        if (pref.getprofile_pic() != null) {
            if (!pref.getprofile_pic().equals("")) {
                Picasso.with(getActivity()).load(pref.getprofile_pic())
                        .placeholder(R.drawable.default_profile)
                        .into(img_profile);
            }
        }

        if (pref.getfirstname() != null) {
            if (!pref.getfirstname().equals("")) {
                et_name.setText(pref.getfirstname());
              //  et_name.setText(pref.getfirstname() + " "+ pref.getlastname());
            }
        }

        if (pref.getnumber() != null) {
            if (!pref.getnumber().equals("")) {
                et_phone.setText(pref.getnumber());
            }
        }

        tv_total_ride.setText(String.valueOf(pref.gettotal_rides()) + " " + "times");

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        Log.d("TAG" , "TOKEN" + pref.gettoken());

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        img_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.pasenger_profile_to_order_fragment);
            }
        });

        ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                navController.navigate(R.id.pasenger_profile_to_chat_fragment);

            }
        });

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.setlogin("0");
                pref.setfirstname("");
                pref.setlastname("");
                pref.setemail("");
                pref.setrole("");
                pref.setcar_model("");
                pref.setcar_ownership_doc("");
                pref.setcountry_code("");
                pref.setdriver_licence("");
                pref.setprofile_pic("");
                pref.setauthtoke("");
                pref.setuserid(0);
                pref.setimagepath("");
                navController.navigate(R.id.pasenger_profile_to_splash3_fragment);
            }
        });


        img_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_PHOTO);

            }
        });

        img_editname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_name.setEnabled(true);
                et_name.setFocusable(true);
                et_name.requestFocus();
                et_name.setCursorVisible(true);
                img_editnamesave.setVisibility(View.VISIBLE);
                img_editname.setVisibility(View.GONE);
            }
        });

        img_editnumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_phone.setEnabled(true);
                et_phone.setFocusable(true);
                et_phone.requestFocus();
                et_phone.setCursorVisible(true);
                img_editnumbersave.setVisibility(View.VISIBLE);
                img_editnumber.setVisibility(View.GONE);
            }
        });

        img_editnamesave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.setpassengername(et_name.getText().toString());
                img_editnamesave.setVisibility(View.GONE);
                img_editname.setVisibility(View.VISIBLE);
                et_name.setEnabled(true);
                et_name.clearFocus();
                et_name.requestFocus();
                et_name.setCursorVisible(false);
                hideSoftKeyboard(v);
                updatepassenger();

            }
        });

      /*  img_editnumbersave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.setnumber(et_phone.getText().toString());
                img_editnumbersave.setVisibility(View.GONE);
                img_editnumber.setVisibility(View.VISIBLE);
                et_phone.setEnabled(true);
                et_phone.clearFocus();
                et_phone.requestFocus();
                et_phone.setCursorVisible(false);
                hideSoftKeyboard(v);
                updatepassenger();
            }
        });
*/
    }

    private void updatepassenger() {

        if (et_name.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Please ypur name", Toast.LENGTH_SHORT).show();
        }

        TypedFile uploadlicens = null;

        if (getFilePath() != null) {
            pref.setimagepath(FilePath.getPath(getActivity(), getFilePath()));
            uploadlicens = new TypedFile("multipart/form-data",  new File(FilePath.getPath(getActivity(), getFilePath())));
        }else {
            uploadlicens = new TypedFile("multipart/form-data",  new File(pref.getimagepath()));
        }

        if (Utils.isNetworkAvailable(getActivity())) {
            Utils.showProgress1("Please wait", getActivity());
            CaberWSapi api = APIService.createService(CaberWSapi.class);
            api.passenger_update_profile(
                    pref.getuserid(),
                    "Token "+ pref.getauthtoke(),
                    et_name.getText().toString(),
                    "",
                    pref.getemail(),
                    pref.gettoken(),
                    "android",
                    uploadlicens,
                    new Callback<Passenger_update_Model>() {
                        @Override
                        public void success(Passenger_update_Model otp_valid_model, Response response) {
                            Utils.hideProgress1();
                            pref.setfirstname(otp_valid_model.getFirst_name());
                            pref.setlastname(otp_valid_model.getLast_name());
                            pref.setnumber(otp_valid_model.getPhone_number());
                            pref.setemail(otp_valid_model.getEmail());
                            pref.setprofile_pic(otp_valid_model.getProfile_pic());
                            pref.setrole(otp_valid_model.getRole());
                            pref.setauthtoke(otp_valid_model.getAuth_token());
                            pref.settoken(otp_valid_model.getDevice_token());
                            pref.gsettotal_rides(otp_valid_model.getTotal_rides());

                            et_name.setText(otp_valid_model.getFirst_name());
                            et_phone.setText(otp_valid_model.getPhone_number());
                            tv_total_ride.setText(String.valueOf(otp_valid_model.getTotal_rides()));

                            Picasso.with(getActivity()).load(otp_valid_model.getProfile_pic())
                                    .placeholder(R.drawable.default_profile)
                                    .into(img_profile);

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                        }
                    });



        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }


    }

    private void hideSoftKeyboard(View v){
        InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void addMarker(double lat, double lng) {
        this.googleMap.clear();
        this.googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10));
    }

    private void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        addMarker(22.5726, 88.3639);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = this.googleMap.addMarker(markerOptions);

        //move map camera
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
    }

    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_PHOTO) {

                setFilePath(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());

                setFilePath(uriFromPath);

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream );
                    img_profile.setImageBitmap(selectedImage);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
                updatepassenger();


            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        et_phone.clearFocus();
        et_name.clearFocus();
    }


  /*  private void sendotp() {
        if (Utils.isNetworkAvailable(getActivity())) {
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.send_otp(
                        "" +
                                "+91"+et_mobile.getText().toString(),
                        new Callback<Splash3_Send_Opt_Model>() {
                            @Override
                            public void success(Splash3_Send_Opt_Model splash3_send_opt_model, Response response) {

                                Utils.hideProgress1();

                                String str_otp =  splash3_send_opt_model.getOtp();
                                String str_pnumber =  splash3_send_opt_model.getPhone_number();
                                String str_pk =  splash3_send_opt_model.getPk();

                                pref.setnumber(str_pnumber);
                                pref.setotp(str_otp);
                                pref.setpk(str_pk);

                                navController.navigate(R.id.splash3_fragment_to_otp_fragment);
                            }

                            @Override
                            public void failure(RetrofitError error) {

                                Toast.makeText(getActivity(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }
                        });



        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }

    }*/

}


