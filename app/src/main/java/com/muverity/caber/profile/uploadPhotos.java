package com.muverity.caber.profile;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.muverity.caber.MyApplication;
import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.AppConst;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.FilePath;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.DriverUpdateProfile_Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static android.content.Context.MODE_PRIVATE;

public class uploadPhotos extends Fragment {

    ImageView img_profile , img_upload , img_car1_save , img_car2_save  , img_car1 , img_car2;
    LinearLayout ll1 , ll2;
    TextView tv_done;
    private int SELECT_PROFILE = 1;
    private int SELECT_CAR1 = 2;
    private int SELECT_CAR2 = 3;

    private Uri filePath;
    private Uri filePath2;
    private Uri filePath3;

    String fileType = AppConst.FILE_TYPE_TEXT, fileExtention = "NULL", fileURL = "NULL", fileDeviceURL = "NULL", messageText = "NULL";
    PreferenceUtils pref;

    EditText et_name ;
    TextView tv_totalride ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.upload_pics, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        img_profile = view.findViewById(R.id.img_profile);
        img_upload = view.findViewById(R.id.img_upload);
        img_car1_save = view.findViewById(R.id.img_car1_save);
        img_car2_save = view.findViewById(R.id.img_car2_save);
        ll1 = view.findViewById(R.id.ll1);
        ll2 = view.findViewById(R.id.ll2);
        img_car1 = view.findViewById(R.id.img_car1);
        img_car2 = view.findViewById(R.id.img_car2);
        tv_done = view.findViewById(R.id.tv_done);
        et_name = view.findViewById(R.id.et_name);
        tv_totalride = view.findViewById(R.id.tv_totalride);
        pref = new PreferenceUtils(getActivity());

        if (pref.getfirstname() != null) {
            if (!pref.getfirstname().equals("")) {
                et_name.setText(pref.getfirstname());
            }
        }

        tv_totalride.setText(String.valueOf(pref.gettotal_rides()));


        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_PROFILE);
            }
        });

        img_car1_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_CAR1);
            }
        });

        img_car2_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_CAR2);
            }
        });

        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TypedFile uploadprofle = null;
                TypedFile uploadcar1 = null;
                TypedFile uploadcar2 = null;

                if (getFilePath3() == null) {
                    Toast.makeText(MyApplication.getAppContext(), "Please profile photo", Toast.LENGTH_SHORT).show();
                }else if  (getFilePath() == null) {
                    Toast.makeText(MyApplication.getAppContext(), "Please upload first car photo", Toast.LENGTH_SHORT).show();
                }else if (getFilePath2() == null) {
                    Toast.makeText(MyApplication.getAppContext(), "Please upload second car photo", Toast.LENGTH_SHORT).show();
                }else {

                    if (getFilePath3() != null) {
                        uploadprofle = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), getFilePath3())));
                    }
                    if  (getFilePath() != null) {
                        uploadcar1 = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), getFilePath())));
                    }
                    if (getFilePath2() != null) {
                        uploadcar2 = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), getFilePath2())));
                    }

                    if (Utils.isNetworkAvailable(getActivity())) {

                        Utils.showProgress1("Please wait", getActivity());
                        CaberWSapi api = APIService.createService(CaberWSapi.class);
                        api.driver_update_profile(
                                pref.getuserid(),
                                "Token "+ pref.getauthtoke(),
                                et_name.getText().toString(),
                                "",
                                pref.getemail(),
                                "android",
                                pref.gettoken(),
                                uploadprofle,
                                uploadcar1,
                                uploadcar2,
                                new Callback<DriverUpdateProfile_Model>() {
                                    @Override
                                    public void success(DriverUpdateProfile_Model otp_valid_model, Response response) {

                                        Utils.hideProgress1();
                                        Toast.makeText(MyApplication.getAppContext(), "Successfully uploaded...", Toast.LENGTH_SHORT).show();

                                        Gson gson = new Gson();
                                        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("USER",MODE_PRIVATE);
                                        String json = gson.toJson((ArrayList) otp_valid_model.getCarPic());
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("Set",json );
                                        editor.commit();

                                        Gson gson2 = new Gson();
                                        String json_cars = sharedPreferences.getString("Set", "");

                                        Log.e("TAG" , "TEST_DATA" + json_cars) ;
                                      /*  if (json_cars.isEmpty()) {
                                            Toast.makeText(getActivity(),"There is something error",Toast.LENGTH_LONG).show();
                                        } else {
                                            Type type = new TypeToken<List<String>>() {
                                            }.getType();
                                            List<String> arrPackageData = gson2.fromJson(json_cars, type);
                                            for(String data:arrPackageData) {
                                               Log.e("TAG" , "TEST_DATA" + data) ;
                                            }
                                        }*/


                                      pref.setprofilestatus("1");
                                      pref.setcarstatus("1");
                                      pref.setprofile_pic(otp_valid_model.getProfilePic());
                                      pref.setnumber(String.valueOf(otp_valid_model.getPhoneNumber()));
                                      pref.settotal_rating(otp_valid_model.getTotalRating());
                                      navController.navigate(R.id.upload_pics_fragment_to_order_fragment);

                                    }
                                    @Override
                                    public void failure(RetrofitError error) {
                                        Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                    }
                                });



                    } else {
                        Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

    }



    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }

    public Uri getFilePath2() {
        return filePath2;
    }

    public void setFilePath2(Uri filePath2) {
        this.filePath2 = filePath2;
    }

    public Uri getFilePath3() {
        return filePath3;
    }

    public void setFilePath3(Uri filePath3) {
        this.filePath3 = filePath3;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_CAR1) {

                setFilePath(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());
                setFilePath(uriFromPath);

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ll1.setVisibility(View.GONE);
                    img_car1.setImageBitmap(selectedImage);
                    img_car1.setVisibility(View.VISIBLE);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }

            }else if (requestCode == SELECT_CAR2){

                setFilePath2(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());
                setFilePath2(uriFromPath);

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ll2.setVisibility(View.GONE);
                    img_car2.setImageBitmap(selectedImage);
                    img_car2.setVisibility(View.VISIBLE);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }


            }else {

                setFilePath3(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());
                setFilePath3(uriFromPath);

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    img_profile.setImageBitmap(selectedImage);
                    img_upload.setVisibility(View.GONE);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }

            }

        }

    }

}
