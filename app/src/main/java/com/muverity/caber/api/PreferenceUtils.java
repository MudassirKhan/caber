package com.muverity.caber.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.muverity.caber.model.DriverUpdateProfile_Model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PreferenceUtils {

    SharedPreferences preferences;
    Context context;

    public PreferenceUtils(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
    }

    public void setotp(String str_otp) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("str_otp", str_otp);
        editor.commit();
    }

    public String getotp() {
        return preferences.getString("str_otp", "");
    }

    public void setnumber(String str_pnumber) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("str_pnumber", str_pnumber);
        editor.commit();
    }

    public String getnumber() {
        return preferences.getString("str_pnumber", "");
    }

    public void setpk(String str_pk) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("str_pk", str_pk);
        editor.commit();
    }

    public String getpk() {
        return preferences.getString("str_pk", "");
    }


    ///user info edit here

    public void setpassengername(String passenger_name) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("passenger_name", passenger_name);
        editor.commit();
    }

    public String getpassengername() {
        return preferences.getString("passenger_name", "");
    }


    public void setfirstname(String firstname) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("firstname", firstname);
        editor.commit();
    }

    public String getfirstname() {
        return preferences.getString("firstname", "");
    }

    public void setlastname(String lastname) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lastname", lastname);
        editor.commit();
    }

    public String getlastname() {
        return preferences.getString("lastname", "");
    }

    public void setemail(String email) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("email", email);
        editor.commit();
    }

    public String getemail() {
        return preferences.getString("email", "");
    }

    public void setcountry_code(String country_code) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("country_code", country_code);
        editor.commit();
    }

    public String getcountry_code() {
        return preferences.getString("country_code", "");
    }


    public void setcar_model(String car_model) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("car_model", car_model);
        editor.commit();
    }

    public String getcar_model() {
        return preferences.getString("car_model", "");
    }

    public void setdriver_licence(String driver_licence) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("driver_licence", driver_licence);
        editor.commit();
    }

    public String getdriver_licence() {
        return preferences.getString("driver_licence", "");
    }

    public void setcar_ownership_doc(String driver_licence) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("driver_licence", driver_licence);
        editor.commit();
    }

    public String getcar_ownership_doc() {
        return preferences.getString("driver_licence", "");
    }

    public void setprofile_pic(String profile_pic) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("profile_pic", profile_pic);
        editor.commit();
    }

    public String getprofile_pic() {
        return preferences.getString("profile_pic", "");
    }

    public void setrole(String role) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("role", role);
        editor.commit();
    }

    public String getrole() {
        return preferences.getString("role", "");
    }





    public void setcars(ArrayList arrayList) {



        SharedPreferences.Editor editor = preferences.edit();
        Set<String> set = new HashSet<String>();
        set.addAll(arrayList);
        editor.putStringSet("carslist", set);
        editor.apply();
    }

    public List<String> getcars() {
        Set<String> set = preferences.getStringSet("carslist", null);
        List<String> sample=new ArrayList<String>(set);
        return sample;
    }

    public void setlogin(String login) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("login", login);
        editor.commit();
    }

    public String getlogin() {
        return preferences.getString("login", "");
    }

    public void settoken(String token) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("token", token);
        editor.commit();
    }

    public String gettoken() {
        return preferences.getString("token", "");
    }


    public void setcartype(String cartype) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("cartype", cartype);
        editor.commit();
    }

    public String getcartype() {
        return preferences.getString("cartype", "");
    }

    public void setauthtoke(String authtoke) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("authtoke", authtoke);
        editor.commit();
    }

    public String getauthtoke() {
        return preferences.getString("authtoke", "");
    }

    public void gsettotal_rides(int total_rides) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("total_rides", total_rides);
        editor.commit();
    }

    public int gettotal_rides() {
        return preferences.getInt("total_rides", 0);
    }

    public void setuserid(int user_id) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("user_id", user_id);
        editor.commit();
    }

    public int getuserid() {
        return preferences.getInt("user_id",0 );
    }

    public void setimagepath(String imagepath) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("imagepath", imagepath);
        editor.commit();
    }

    public String getimagepath() {
        return preferences.getString("imagepath", "");
    }

    public void settotal_rating(double rating) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("rating", String.valueOf(rating));
        editor.commit();
    }

    public String getsettotal_rating() {
        return preferences.getString("rating", "");
    }

    public void setprofilestatus(String profilestatus) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("profilestatus", String.valueOf(profilestatus));
        editor.commit();
    }

    public String getprofilestatus() {
        return preferences.getString("profilestatus", "");
    }

    public void setcarstatus(String carstatus) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("carstatus", String.valueOf(carstatus));
        editor.commit();
    }

    public String getcarstatus() {
        return preferences.getString("carstatus", "");
    }

    public void settransid(int transid) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("transid", String.valueOf(transid));
        editor.commit();
    }

    public String gettransid() {
        return preferences.getString("transid", "");
    }

}
