package com.muverity.caber.api;

import com.muverity.caber.BuildConfig;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.UrlConnectionClient;


public class APIService {
    public APIService() {
    }

     public static <S> S createService(Class<S> serviceClass) {

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(AppConst.BASE_URL)
                .setClient(getClient())
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE);
        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }

    private static Client getClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS);
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setWriteTimeout(60, TimeUnit.SECONDS);
//        client.setRetryOnConnectionFailure(true);
        return new UrlConnectionClient();
    }




}


   /* public static <S> S createService(Class<S> serviceClass) {

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(AppConst.BASE_URL)
                .setClient(getClient())
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE);
        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }




    private static Client getClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS);
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setWriteTimeout(60, TimeUnit.SECONDS);
//        client.setRetryOnConnectionFailure(true);
        return new UrlConnectionClient();
    }*/
