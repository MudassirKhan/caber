package com.muverity.caber.api;

import com.muverity.caber.model.Accetpt_Ride_Model;
import com.muverity.caber.model.ContactListModel;
import com.muverity.caber.model.Create_Ride_Model;
import com.muverity.caber.model.DriverNotification_Model;
import com.muverity.caber.model.DriverUpdateProfile_Model;
import com.muverity.caber.model.Order_Model;
import com.muverity.caber.model.Otp_Valid_Model;
import com.muverity.caber.model.Passenger_update_Model;
import com.muverity.caber.model.PlansModel;
import com.muverity.caber.model.PlansPurchaseModel;
import com.muverity.caber.model.Splash3_Send_Opt_Model;
import com.muverity.caber.notification.Notification_Model;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedInput;

public interface CaberWSapi {

    @FormUrlEncoded
    @POST("/api/send_otp/")
    void send_otp(
            @Field("phone_number") String phone_number,
            Callback<Splash3_Send_Opt_Model> updateResponse);

  /*  @Headers("Content-Type: application/json")
    @POST("/api/validate_otp/")
    void validate_otp(
            @Header("pk") String pk,
            @Header("otp") String otp,
            @Header("email") String email,
            @Header("device_token") String device_token,
            @Header("device_type") String device_type,
            @Header("role") int role,
            Callback<Otp_Valid_Model> updateResponse);


    @Headers("Content-Type: application/json")
    @POST("/api/validate_otp/")
    Callback<Otp_Valid_Model> validate_otp(
            @Body String body,
            Callback<Otp_Valid_Model> connection_refused);

    @Headers("Content-Type: application/json")
    @POST("/api/validate_otp/")
    void validate_otp(
            @Body TypedInput body,
            Callback<Otp_Valid_Model> callback);*/

   /* @Headers("Content-Type: application/json")
    @POST("/api/validate_otp/")
    public void validate_otp(
            @Body TypedInput body,
            Callback<Otp_Valid_Model> callback);*/

    @Headers("Content-Type: application/json")
    @POST("/api/user_signup/")
    public void validate_otp(
            @Body TypedInput body,
            Callback<Otp_Valid_Model> callback);

    @Headers("Content-Type: application/json")
    @POST("/api/create_ride/")
    public void create_ride(
            @Header("Authorization") String authorization,
            @Body TypedInput body,
            Callback<Create_Ride_Model> callback);

   /* @FormUrlEncoded
    @POST("/api/create_ride/")
    void create_ride(
            @Header("Authorization") String authorization,
            @Field("car_type") String car_type,
            @Field("budget") String budget,
            @Field("pickup_name") String pickup_name,
            @Field("pickup_address") String pickup_address,
            @Field("pickup_latitude") String pickup_latitude,
            @Field("pickup_longitude") String pickup_longitude,
            @Field("drop_name") String drop_name,
            @Field("drop_address") String drop_address,
            @Field("drop_latitude") String drop_latitude,
            @Field("drop_longitude") String drop_longitude,
            @Field("rideType") String rideType,
            Callback<Create_Ride_Model> updateResponse);*/

    @FormUrlEncoded
    @POST("/api/accept_ride/")
    void accept_ride(
            @Header("Authorization") String authorization,
            @Field("ride_id") int ride_id,
            @Field("accepted_budget") String accepted_budget,
            Callback<Accetpt_Ride_Model> updateResponse);

    @Multipart
    @PUT("/api/user/"+"{id}/")
    void passenger_update_profile(
            @Path("id") int id,
            @Header("Authorization") String authorization,
            @Part("first_name") String first_name,
            @Part("last_name") String last_name,
            @Part("email") String email,
            @Part("device_token") String device_token,
            @Part("device_type") String device_type,
            @Part("profile_pic") TypedFile picture,
            Callback<Passenger_update_Model> updateResponse);


    @Multipart
    @PUT("/api/user/"+"{id}/")
    void driver_update_profile(
            @Path("id") int id,
            @Header("Authorization") String authorization,
            @Part("first_name") String first_name,
            @Part("last_name") String last_name,
            @Part("email") String email,
            @Part("device_type") String device_type,
            @Part("device_token") String device_token,
            @Part("profile_pic") TypedFile profile_pic,
            @Part("pic") TypedFile picture1,
            @Part("pic") TypedFile picture2,
            Callback<DriverUpdateProfile_Model> updateResponse);

   /* @Multipart
    @POST("/api/validate_otp/")
    void driver_singup_profile(
            @Part("device_token") String device_token,
            @Part("pk") String pk,
            @Part("otp") String otp,
            @Part("device_type") String device_type,
            @Part("role") String role,
            @Part("first_name") String first_name,
            @Part("last_name") String last_name,
            @Part("car_model") String car_model,
            @Part("driver_licence") TypedFile driver_licence,
            @Part("car_ownership_doc") TypedFile car_ownership_doc,
            Callback<Otp_Valid_Model> updateResponse);*/


    @Multipart
    @POST("/api/user_signup/")
    void driver_singup_profile(
            @Part("phone_number") String phone_number,
            @Part("email") String email,
            @Part("first_name") String first_name,
            @Part("car_type") String car_type,
            @Part("role") int role,
            @Part("car_model") String car_model,
            @Part("device_token") String device_token,
            @Part("device_type") String device_type,
            Callback<Otp_Valid_Model> updateResponse);

   /* @Headers("Content-Type: application/json")
    @POST("/api/validate_otp/")
    public void driver_singup_profile(
            @Body TypedInput body,
            Callback<Otp_Valid_Model> callback);*/

    @GET("/api/notification_list/")
    void notification_list(
            @Header("Authorization") String authorization,
            Callback<ArrayList<DriverNotification_Model>> updateResponse);

    @GET("/api/ride_accepted_drivers/")
    void list_of_accepted_driver(
            @Header("Authorization") String authorization,
            @Query("ride_id") int ride_id,
            Callback< ArrayList<Accetpt_Ride_Model>> updateResponse);


    @GET("/api/order_list/")
    void current_previous_list(
            @Header("Authorization") String authorization,
            Callback<Order_Model> updateResponse);

    @GET("/api/notification_list/")
    void passenger_notification_list(
            @Header("Authorization") String authorization,
            Callback<ArrayList<Notification_Model>> updateResponse);


    @GET("/api/contact_list/")
    void contact_list(
            @Header("Authorization") String authorization,
            Callback<ArrayList<ContactListModel>> updateResponse);

    @POST("/api/accept_ride_passenger /")
    void passenger_accept(
            @Header("Authorization") String authorization,
            @Body TypedInput body,
            Callback<Create_Ride_Model> updateResponse);

    @GET("/api/plans/")
    void plans(Callback<ArrayList<PlansModel>> updateResponse);

    @Headers("Content-Type: application/json")
    @POST("/api/purchase_plans/")
    public void purchase_plans(
            @Header("Authorization") String authorization,
            @Body TypedInput body,
            Callback<PlansPurchaseModel> callback);
}
