package com.muverity.caber.model;

public class Chat_Model {

    private String member_name;
    private String member_title;
    private int member_image;

    public Chat_Model(String member_name, String member_title, int member_image) {
        this.member_name = member_name;
        this.member_title = member_title;
        this.member_image = member_image;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public String getMember_title() {
        return member_title;
    }

    public void setMember_title(String member_title) {
        this.member_title = member_title;
    }

    public int getMember_image() {
        return member_image;
    }

    public void setMember_image(int member_image) {
        this.member_image = member_image;
    }
}
