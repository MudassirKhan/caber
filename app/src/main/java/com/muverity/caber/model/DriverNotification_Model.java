package com.muverity.caber.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DriverNotification_Model implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("notification_text")
    @Expose
    private String notificationText;
    @SerializedName("passenger_details")
    @Expose
    private PassengerDetails passengerDetails;
    @SerializedName("ride_details")
    @Expose
    private RideDetails rideDetails;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("driver_details")
    @Expose
    private DriverDetails driverDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public PassengerDetails getPassengerDetails() {
        return passengerDetails;
    }

    public void setPassengerDetails(PassengerDetails passengerDetails) {
        this.passengerDetails = passengerDetails;
    }

    public RideDetails getRideDetails() {
        return rideDetails;
    }

    public void setRideDetails(RideDetails rideDetails) {
        this.rideDetails = rideDetails;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public DriverDetails getDriverDetails() {
        return driverDetails;
    }

    public void setDriverDetails(DriverDetails driverDetails) {
        this.driverDetails = driverDetails;
    }

    public class PassengerDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private Object lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("country_code")
        @Expose
        private String countryCode;

        @SerializedName("profile_pic")
        @Expose
        private String profilePic;

        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("auth_token")
        @Expose
        private String authToken;
        @SerializedName("role")
        @Expose
        private Integer role;
        @SerializedName("total_rides")
        @Expose
        private Integer totalRides;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public Object getLastName() {
            return lastName;
        }

        public void setLastName(Object lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public Integer getRole() {
            return role;
        }

        public void setRole(Integer role) {
            this.role = role;
        }

        public Integer getTotalRides() {
            return totalRides;
        }

        public void setTotalRides(Integer totalRides) {
            this.totalRides = totalRides;
        }

    }


    public class RideDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("passenger")
        @Expose
        private Integer passenger;
        @SerializedName("car_type")
        @Expose
        private String carType;
        @SerializedName("budget")
        @Expose
        private String budget;
        @SerializedName("pickup_name")
        @Expose
        private String pickupName;
        @SerializedName("pickup_address")
        @Expose
        private String pickupAddress;
        @SerializedName("pickup_latitude")
        @Expose
        private String pickupLatitude;
        @SerializedName("pickup_longitude")
        @Expose
        private String pickupLongitude;
        @SerializedName("drop_name")
        @Expose
        private String dropName;
        @SerializedName("drop_address")
        @Expose
        private String dropAddress;
        @SerializedName("drop_latitude")
        @Expose
        private String dropLatitude;
        @SerializedName("drop_longitude")
        @Expose
        private String dropLongitude;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("ride_type")
        @Expose
        private String rideType;
        @SerializedName("status")
        @Expose
        private String status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPassenger() {
            return passenger;
        }

        public void setPassenger(Integer passenger) {
            this.passenger = passenger;
        }

        public String getCarType() {
            return carType;
        }

        public void setCarType(String carType) {
            this.carType = carType;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getPickupName() {
            return pickupName;
        }

        public void setPickupName(String pickupName) {
            this.pickupName = pickupName;
        }

        public String getPickupAddress() {
            return pickupAddress;
        }

        public void setPickupAddress(String pickupAddress) {
            this.pickupAddress = pickupAddress;
        }

        public String getPickupLatitude() {
            return pickupLatitude;
        }

        public void setPickupLatitude(String pickupLatitude) {
            this.pickupLatitude = pickupLatitude;
        }

        public String getPickupLongitude() {
            return pickupLongitude;
        }

        public void setPickupLongitude(String pickupLongitude) {
            this.pickupLongitude = pickupLongitude;
        }

        public String getDropName() {
            return dropName;
        }

        public void setDropName(String dropName) {
            this.dropName = dropName;
        }

        public String getDropAddress() {
            return dropAddress;
        }

        public void setDropAddress(String dropAddress) {
            this.dropAddress = dropAddress;
        }

        public String getDropLatitude() {
            return dropLatitude;
        }

        public void setDropLatitude(String dropLatitude) {
            this.dropLatitude = dropLatitude;
        }

        public String getDropLongitude() {
            return dropLongitude;
        }

        public void setDropLongitude(String dropLongitude) {
            this.dropLongitude = dropLongitude;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getRideType() {
            return rideType;
        }

        public void setRideType(String rideType) {
            this.rideType = rideType;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }

    public class DriverDetails {


    }

}
