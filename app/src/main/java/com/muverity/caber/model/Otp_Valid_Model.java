package com.muverity.caber.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Otp_Valid_Model {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("first_name")
    @Expose
    private String first_name;

    @SerializedName("last_name")
    @Expose
    private String last_name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("phone_number")
    @Expose
    private String phone_number;

    @SerializedName("country_code")
    @Expose
    private String country_code;

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    @SerializedName("car_model")
    @Expose
    private String car_model;

    @SerializedName("driver_licence")
    @Expose
    private String driver_licence;

    @SerializedName("car_ownership_doc")
    @Expose
    private String car_ownership_doc;

    @SerializedName("profile_pic")
    @Expose
    private String profile_pic;

    @SerializedName("device_type")
    @Expose
    private String device_type;

    @SerializedName("device_token")
    @Expose
    private String device_token;

    @SerializedName("auth_token")
    @Expose
    private String auth_token;

    @SerializedName("role")
    @Expose
    private String role;

    @SerializedName("car_type")
    @Expose
    private String car_type;

    @SerializedName("total_rides")
    @Expose
    private int total_rides;

    @SerializedName("total_rating")
    @Expose
    private double total_rating;

    @SerializedName("car_pic")
    @Expose
    private List<Object> carPic = null;

    public List<Object> getCarPic() {
        return carPic;
    }

    public void setCarPic(List<Object> carPic) {
        this.carPic = carPic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getCar_type() {
        return car_type;
    }

    public void setCar_type(String car_type) {
        this.car_type = car_type;
    }

    public int getTotal_rides() {
        return total_rides;
    }

    public void setTotal_rides(int total_rides) {
        this.total_rides = total_rides;
    }

    public double getTotal_rating() {
        return total_rating;
    }

    public void setTotal_rating(double total_rating) {
        this.total_rating = total_rating;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCar_model() {
        return car_model;
    }

    public void setCar_model(String car_model) {
        this.car_model = car_model;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }



    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getDriver_licence() {
        return driver_licence;
    }

    public void setDriver_licence(String driver_licence) {
        this.driver_licence = driver_licence;
    }

    public String getCar_ownership_doc() {
        return car_ownership_doc;
    }

    public void setCar_ownership_doc(String car_ownership_doc) {
        this.car_ownership_doc = car_ownership_doc;
    }


}
