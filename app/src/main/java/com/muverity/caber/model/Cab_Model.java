package com.muverity.caber.model;

public class Cab_Model {

    private String car_name;
    private int car_image;

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public int getCar_image() {
        return car_image;
    }

    public void setCar_image(int car_image) {
        this.car_image = car_image;
    }

    public Cab_Model(String car_name, int car_image) {
        this.car_name = car_name;
        this.car_image = car_image;
    }
}
