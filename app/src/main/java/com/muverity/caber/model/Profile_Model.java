package com.muverity.caber.model;

public class Profile_Model {

    private int imgId;

    public Profile_Model(int imgId) {
        this.imgId = imgId;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}
