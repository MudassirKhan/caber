package com.muverity.caber.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ContactListModel {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("first_name")
    @Expose
    private Integer first_name;

    @SerializedName("last_name")
    @Expose
    private Integer last_name;

    @SerializedName("email")
    @Expose
    private Integer email;

    @SerializedName("phone_number")
    @Expose
    private Integer phone_number;

    @SerializedName("country_code")
    @Expose
    private Integer country_code;

    @SerializedName("car_model")
    @Expose
    private Integer car_model;

    @SerializedName("driver_licence")
    @Expose
    private Integer driver_licence;

    @SerializedName("car_ownership_doc")
    @Expose
    private Integer car_ownership_doc;

    @SerializedName("profile_pic")
    @Expose
    private Integer profile_pic;

    @SerializedName("device_type")
    @Expose
    private Integer device_type;

    @SerializedName("device_token")
    @Expose
    private Integer device_token;

    @SerializedName("auth_token")
    @Expose
    private Integer auth_token;

    @SerializedName("car_pic")
    @Expose
    private ArrayList<String> car_pic;

    @SerializedName("role")
    @Expose
    private Integer role;

    @SerializedName("car_type")
    @Expose
    private Integer car_type;

    @SerializedName("total_rides")
    @Expose
    private Integer total_rides;

    @SerializedName("total_rating")
    @Expose
    private Integer total_rating;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFirst_name() {
        return first_name;
    }

    public void setFirst_name(Integer first_name) {
        this.first_name = first_name;
    }

    public Integer getLast_name() {
        return last_name;
    }

    public void setLast_name(Integer last_name) {
        this.last_name = last_name;
    }

    public Integer getEmail() {
        return email;
    }

    public void setEmail(Integer email) {
        this.email = email;
    }

    public Integer getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(Integer phone_number) {
        this.phone_number = phone_number;
    }

    public Integer getCountry_code() {
        return country_code;
    }

    public void setCountry_code(Integer country_code) {
        this.country_code = country_code;
    }

    public Integer getCar_model() {
        return car_model;
    }

    public void setCar_model(Integer car_model) {
        this.car_model = car_model;
    }

    public Integer getDriver_licence() {
        return driver_licence;
    }

    public void setDriver_licence(Integer driver_licence) {
        this.driver_licence = driver_licence;
    }

    public Integer getCar_ownership_doc() {
        return car_ownership_doc;
    }

    public void setCar_ownership_doc(Integer car_ownership_doc) {
        this.car_ownership_doc = car_ownership_doc;
    }

    public Integer getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(Integer profile_pic) {
        this.profile_pic = profile_pic;
    }

    public Integer getDevice_type() {
        return device_type;
    }

    public void setDevice_type(Integer device_type) {
        this.device_type = device_type;
    }

    public Integer getDevice_token() {
        return device_token;
    }

    public void setDevice_token(Integer device_token) {
        this.device_token = device_token;
    }

    public Integer getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(Integer auth_token) {
        this.auth_token = auth_token;
    }

    public ArrayList<String> getCar_pic() {
        return car_pic;
    }

    public void setCar_pic(ArrayList<String> car_pic) {
        this.car_pic = car_pic;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getCar_type() {
        return car_type;
    }

    public void setCar_type(Integer car_type) {
        this.car_type = car_type;
    }

    public Integer getTotal_rides() {
        return total_rides;
    }

    public void setTotal_rides(Integer total_rides) {
        this.total_rides = total_rides;
    }

    public Integer getTotal_rating() {
        return total_rating;
    }

    public void setTotal_rating(Integer total_rating) {
        this.total_rating = total_rating;
    }
}
