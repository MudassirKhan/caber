package com.muverity.caber.model;

public class Request_Result_Model  {

    private String car_member_name;
    private String car_name;
    private String price;
    private int car_image;
    private int car_member_image;

    public Request_Result_Model(String car_member_name, String car_name, String price, int car_image, int car_member_image) {
        this.car_member_name = car_member_name;
        this.car_name = car_name;
        this.price = price;
        this.car_image = car_image;
        this.car_member_image = car_member_image;
    }

    public String getCar_member_name() {
        return car_member_name;
    }

    public void setCar_member_name(String car_member_name) {
        this.car_member_name = car_member_name;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getCar_image() {
        return car_image;
    }

    public void setCar_image(int car_image) {
        this.car_image = car_image;
    }

    public int getCar_member_image() {
        return car_member_image;
    }

    public void setCar_member_image(int car_member_image) {
        this.car_member_image = car_member_image;
    }
}
