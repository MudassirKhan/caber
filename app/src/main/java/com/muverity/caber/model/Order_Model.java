package com.muverity.caber.model;

import java.util.ArrayList;

public class Order_Model {

    private ArrayList<current_ride_info> current_ride;
    private ArrayList<previous_ride_info> previous_ride;

    public ArrayList<current_ride_info> getCurrent_ride() {
        return current_ride;
    }

    public void setCurrent_ride(ArrayList<current_ride_info> current_ride) {
        this.current_ride = current_ride;
    }

    public ArrayList<previous_ride_info> getPrevious_ride() {
        return previous_ride;
    }

    public void setPrevious_ride(ArrayList<previous_ride_info> previous_ride) {
        this.previous_ride = previous_ride;
    }

    public class current_ride_info {

        private Integer id;
        private Integer passenger;
        private String car_type;
        private String budget;
        private String pickup_name;
        private String pickup_address;
        private String pickup_latitude;
        private String pickup_longitude;
        private String drop_name;
        private String drop_address;
        private String drop_latitude;
        private String drop_longitude;
        private String created_at;
        private String ride_type;
        private String status;


        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPassenger() {
            return passenger;
        }

        public void setPassenger(Integer passenger) {
            this.passenger = passenger;
        }

        public String getCar_type() {
            return car_type;
        }

        public void setCar_type(String car_type) {
            this.car_type = car_type;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getPickup_name() {
            return pickup_name;
        }

        public void setPickup_name(String pickup_name) {
            this.pickup_name = pickup_name;
        }

        public String getPickup_address() {
            return pickup_address;
        }

        public void setPickup_address(String pickup_address) {
            this.pickup_address = pickup_address;
        }

        public String getPickup_latitude() {
            return pickup_latitude;
        }

        public void setPickup_latitude(String pickup_latitude) {
            this.pickup_latitude = pickup_latitude;
        }

        public String getPickup_longitude() {
            return pickup_longitude;
        }

        public void setPickup_longitude(String pickup_longitude) {
            this.pickup_longitude = pickup_longitude;
        }

        public String getDrop_name() {
            return drop_name;
        }

        public void setDrop_name(String drop_name) {
            this.drop_name = drop_name;
        }

        public String getDrop_address() {
            return drop_address;
        }

        public void setDrop_address(String drop_address) {
            this.drop_address = drop_address;
        }

        public String getDrop_latitude() {
            return drop_latitude;
        }

        public void setDrop_latitude(String drop_latitude) {
            this.drop_latitude = drop_latitude;
        }

        public String getDrop_longitude() {
            return drop_longitude;
        }

        public void setDrop_longitude(String drop_longitude) {
            this.drop_longitude = drop_longitude;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getRide_type() {
            return ride_type;
        }

        public void setRide_type(String ride_type) {
            this.ride_type = ride_type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public class previous_ride_info {

        private Integer id;
        private Integer passenger;
        private String car_type;
        private String budget;
        private String pickup_name;
        private String pickup_address;
        private String pickup_latitude;
        private String pickup_longitude;
        private String drop_name;
        private String drop_address;
        private String drop_latitude;
        private String drop_longitude;
        private String created_at;
        private String ride_type;
        private String status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPassenger() {
            return passenger;
        }

        public void setPassenger(Integer passenger) {
            this.passenger = passenger;
        }

        public String getCar_type() {
            return car_type;
        }

        public void setCar_type(String car_type) {
            this.car_type = car_type;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getPickup_name() {
            return pickup_name;
        }

        public void setPickup_name(String pickup_name) {
            this.pickup_name = pickup_name;
        }

        public String getPickup_address() {
            return pickup_address;
        }

        public void setPickup_address(String pickup_address) {
            this.pickup_address = pickup_address;
        }

        public String getPickup_latitude() {
            return pickup_latitude;
        }

        public void setPickup_latitude(String pickup_latitude) {
            this.pickup_latitude = pickup_latitude;
        }

        public String getPickup_longitude() {
            return pickup_longitude;
        }

        public void setPickup_longitude(String pickup_longitude) {
            this.pickup_longitude = pickup_longitude;
        }

        public String getDrop_name() {
            return drop_name;
        }

        public void setDrop_name(String drop_name) {
            this.drop_name = drop_name;
        }

        public String getDrop_address() {
            return drop_address;
        }

        public void setDrop_address(String drop_address) {
            this.drop_address = drop_address;
        }

        public String getDrop_latitude() {
            return drop_latitude;
        }

        public void setDrop_latitude(String drop_latitude) {
            this.drop_latitude = drop_latitude;
        }

        public String getDrop_longitude() {
            return drop_longitude;
        }

        public void setDrop_longitude(String drop_longitude) {
            this.drop_longitude = drop_longitude;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getRide_type() {
            return ride_type;
        }

        public void setRide_type(String ride_type) {
            this.ride_type = ride_type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
