package com.muverity.caber.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Accetpt_Ride_Model implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("accepted_by")
    @Expose
    private Accetpt_Ride_Model.AcceptedBy accepted_by;

    @SerializedName("accepted_at")
    @Expose
    private String accepted_at;

    @SerializedName("accepted_budget")
    @Expose
    private String accepted_budget;

    @SerializedName("is_accept")
    @Expose
    private String is_accept;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    @SerializedName("ride")
    @Expose
    private Accetpt_Ride_Model.Ride ride;

    public void setAccepted_by(AcceptedBy accepted_by) {
        this.accepted_by = accepted_by;
    }

    public String getAccepted_at() {
        return accepted_at;
    }

    public void setAccepted_at(String accepted_at) {
        this.accepted_at = accepted_at;
    }

    public String getAccepted_budget() {
        return accepted_budget;
    }

    public void setAccepted_budget(String accepted_budget) {
        this.accepted_budget = accepted_budget;
    }

    public String getIs_accept() {
        return is_accept;
    }

    public void setIs_accept(String is_accept) {
        this.is_accept = is_accept;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }



    public Ride getRide() {
        return ride;
    }

    public void setRide(Ride ride) {
        this.ride = ride;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AcceptedBy getAccepted_by() {
        return accepted_by;
    }



    public class AcceptedBy implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("first_name")
        @Expose
        private String first_name;

        @SerializedName("last_name")
        @Expose
        private String last_name;

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("phone_number")
        @Expose
        private String phone_number;

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        @SerializedName("country_code")
        @Expose
        private String country_code;

        @SerializedName("car_model")
        @Expose
        private String car_model;

        @SerializedName("driver_licence")
        @Expose
        private String driver_licence;

        @SerializedName("car_ownership_doc")
        @Expose
        private String car_ownership_doc;

        @SerializedName("profile_pic")
        @Expose
        private String profile_pic;

        @SerializedName("device_type")
        @Expose
        private String device_type;

        @SerializedName("device_token")
        @Expose
        private String device_token;

        @SerializedName("auth_token")
        @Expose
        private String auth_token;

        @SerializedName("car_pic")
        @Expose
        private List<CarPic> carPic = null;



        @SerializedName("role")
        @Expose
        private String role;

        @SerializedName("car_type")
        @Expose
        private String car_type;

        @SerializedName("total_rides")
        @Expose
        private int total_rides;

        public int getTotal_rides() {
            return total_rides;
        }

        public void setTotal_rides(int total_rides) {
            this.total_rides = total_rides;
        }

        public double getTotal_rating() {
            return total_rating;
        }

        public void setTotal_rating(double total_rating) {
            this.total_rating = total_rating;
        }

        @SerializedName("total_rating")
        @Expose
        private double total_rating;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }


        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public List<CarPic> getCarPic() {
            return carPic;
        }

        public void setCarPic(List<CarPic> carPic) {
            this.carPic = carPic;
        }


        public String getCar_model() {
            return car_model;
        }

        public void setCar_model(String car_model) {
            this.car_model = car_model;
        }

        public String getDriver_licence() {
            return driver_licence;
        }

        public void setDriver_licence(String driver_licence) {
            this.driver_licence = driver_licence;
        }

        public String getCar_ownership_doc() {
            return car_ownership_doc;
        }

        public void setCar_ownership_doc(String car_ownership_doc) {
            this.car_ownership_doc = car_ownership_doc;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getAuth_token() {
            return auth_token;
        }

        public void setAuth_token(String auth_token) {
            this.auth_token = auth_token;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getCar_type() {
            return car_type;
        }

        public void setCar_type(String car_type) {
            this.car_type = car_type;
        }

    }

    public class Ride  implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("passenger")
        @Expose
        private Integer passenger;

        @SerializedName("car_type")
        @Expose
        private String car_type;

        @SerializedName("budget")
        @Expose
        private String budget;

        @SerializedName("pickup_name")
        @Expose
        private String pickup_name;

        @SerializedName("pickup_address")
        @Expose
        private String pickup_address;

        @SerializedName("pickup_latitude")
        @Expose
        private String pickup_latitude;

        @SerializedName("pickup_longitude")
        @Expose
        private String pickup_longitude;

        @SerializedName("drop_name")
        @Expose
        private String drop_name;

        @SerializedName("drop_address")
        @Expose
        private String drop_address;

        @SerializedName("drop_latitude")
        @Expose
        private String drop_latitude;

        @SerializedName("drop_longitude")
        @Expose
        private String drop_longitude;

        @SerializedName("created_at")
        @Expose
        private String created_at;

        @SerializedName("ride_type")
        @Expose
        private String ride_type;

        @SerializedName("status")
        @Expose
        private String status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPassenger() {
            return passenger;
        }

        public void setPassenger(Integer passenger) {
            this.passenger = passenger;
        }

        public String getCar_type() {
            return car_type;
        }

        public void setCar_type(String car_type) {
            this.car_type = car_type;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getPickup_name() {
            return pickup_name;
        }

        public void setPickup_name(String pickup_name) {
            this.pickup_name = pickup_name;
        }

        public String getPickup_address() {
            return pickup_address;
        }

        public void setPickup_address(String pickup_address) {
            this.pickup_address = pickup_address;
        }

        public String getPickup_latitude() {
            return pickup_latitude;
        }

        public void setPickup_latitude(String pickup_latitude) {
            this.pickup_latitude = pickup_latitude;
        }

        public String getPickup_longitude() {
            return pickup_longitude;
        }

        public void setPickup_longitude(String pickup_longitude) {
            this.pickup_longitude = pickup_longitude;
        }

        public String getDrop_name() {
            return drop_name;
        }

        public void setDrop_name(String drop_name) {
            this.drop_name = drop_name;
        }

        public String getDrop_address() {
            return drop_address;
        }

        public void setDrop_address(String drop_address) {
            this.drop_address = drop_address;
        }

        public String getDrop_latitude() {
            return drop_latitude;
        }

        public void setDrop_latitude(String drop_latitude) {
            this.drop_latitude = drop_latitude;
        }

        public String getDrop_longitude() {
            return drop_longitude;
        }

        public void setDrop_longitude(String drop_longitude) {
            this.drop_longitude = drop_longitude;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getRide_type() {
            return ride_type;
        }

        public void setRide_type(String ride_type) {
            this.ride_type = ride_type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public class CarPic implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("image")
        @Expose
        private String image;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }

}
