package com.muverity.caber.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.model.Profile_Model;

public class Profile_adapter  extends RecyclerView.Adapter<Profile_adapter.ViewHolder>{

    private Profile_Model[] listdata;

    public Profile_adapter(Profile_Model[] listdata) {
        this.listdata = listdata;
    }
    @NonNull
    @Override
    public Profile_adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.row_profile_car, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Profile_adapter.ViewHolder holder, int position) {

        holder.img_main.setImageResource(listdata[position].getImgId());
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_main;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_main = itemView.findViewById(R.id.img_main);
        }
    }
}
