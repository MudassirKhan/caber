package com.muverity.caber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.model.ContactListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Chat_Adapter extends RecyclerView.Adapter<Chat_Adapter.ViewHolder>{

    ArrayList<ContactListModel> list;
    private Context mCtx;

    public Chat_Adapter(Context mCtx ,  ArrayList<ContactListModel> listdata) {
        this.mCtx = mCtx;
        this.list = listdata;
    }


    @NonNull
    @Override
    public Chat_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.row_chat, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull Chat_Adapter.ViewHolder holder, int position) {


        holder.tv_car_name.setText(list.get(position).getFirst_name());
        holder.tv_name.setText(list.get(position).getCar_model());
        Picasso.with(mCtx)
                .load(list.get(position).getProfile_pic())
                .placeholder(R.drawable.default_profile)
                .into(holder.img_profile);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_profile;
        TextView tv_name , tv_car_name ;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_profile = itemView.findViewById(R.id.img_profile);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_car_name = itemView.findViewById(R.id.tv_car_name);


            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {


        }

    }
}
