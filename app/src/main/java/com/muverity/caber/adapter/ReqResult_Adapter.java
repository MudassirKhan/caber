package com.muverity.caber.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.api.AppConst;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.model.Accetpt_Ride_Model;
import com.muverity.caber.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class ReqResult_Adapter extends RecyclerView.Adapter<ReqResult_Adapter.ViewHolder>{

    ArrayList<Accetpt_Ride_Model> list;
    private Context mCtx;
    int driver_id , ride_id ;
    PreferenceUtils pref;
    public ReqResult_Adapter(Context mCtx , ArrayList<Accetpt_Ride_Model> listdata) {
        this.mCtx = mCtx;
        this.list = listdata;
        pref = new PreferenceUtils(mCtx);
    }

    @NonNull
    @Override
    public ReqResult_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.row_request_result, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ReqResult_Adapter.ViewHolder holder, final int position) {

        holder.tv_name.setText(list.get(position).getAccepted_by().getFirst_name());
        holder.tv_car_name.setText(list.get(position).getAccepted_by().getCar_type());
        holder.tv_price.setText("$"+list.get(position).getAccepted_budget());

        driver_id = list.get(position).getAccepted_by().getId();
        ride_id = list.get(position).getId();

        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle args = new Bundle();

                args.putInt("driver_id", list.get(position).getAccepted_by().getId());
                args.putInt("total_rides", list.get(position).getAccepted_by().getTotal_rides());
                args.putString("number",list.get(position).getAccepted_by().getPhone_number());
                Log.e("TAG" , "TEST_NUM" + list.get(position).getAccepted_by().getPhone_number());
                args.putDouble("total_rating", list.get(position).getAccepted_by().getTotal_rating());
                args.putString("name",list.get(position).getAccepted_by().getFirst_name());
                args.putString("profile",list.get(position).getAccepted_by().getProfile_pic());
                args.putSerializable("list", (Serializable) list.get(position).getAccepted_by().getCarPic());
                args.putInt("ride_id", list.get(position).getRide().getId());
                args.putInt("passenger_id", list.get(position).getRide().getPassenger());
                args.putString("budget", list.get(position).getRide().getBudget());
                args.putString("pick", list.get(position).getRide().getPickup_name());
                args.putString("drop", list.get(position).getRide().getDrop_name());
                args.putString("pick_lat", list.get(position).getRide().getPickup_latitude());
                args.putString("pick_long", list.get(position).getRide().getPickup_longitude());
                args.putString("drop_lat", list.get(position).getRide().getDrop_latitude());
                args.putString("drop_lng", list.get(position).getRide().getDrop_longitude());
                args.putString("ride_type", list.get(position).getRide().getRide_type());
                args.putString("status", list.get(position).getRide().getStatus());


                final NavController navController = Navigation.findNavController((Activity) mCtx, R.id.my_nav_host_fragment);
                navController.navigate(R.id.result_fragment_to_accepted_fragment ,args);

            }
        });


        Picasso.with(mCtx)
                .load(list.get(position).getAccepted_by().getProfile_pic())
                .placeholder(R.drawable.ic_car)
                .into(holder.img_main);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_main , iv_car ;
        TextView tv_view;
        TextView tv_name , tv_car_name , tv_price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_main = itemView.findViewById(R.id.img_main);
            iv_car = itemView.findViewById(R.id.iv_car);
            tv_view = itemView.findViewById(R.id.tv_view);

            tv_car_name = itemView.findViewById(R.id.tv_car_name);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);


        }
       /* @Override
        public void onClick(View view) {

           // final NavController navController = Navigation.findNavController((Activity) mCtx, R.id.my_nav_host_fragment);
          //  navController.navigate(R.id.result_fragment_to_accepted_fragment ,args);

  *//*          String rawdata = getJson();
            TypedInput in = null;
            try {
                in = new TypedByteArray("application/json", rawdata.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {

                if (Utils.isNetworkAvailable(mCtx)) {
                    Utils.showProgress1("Please wait", mCtx);
                    CaberWSapi api = APIService.createService(CaberWSapi.class);
                    api.passenger_accept(
                            "Token "+ pref.getauthtoke(),
                            in,
                            new Callback<Create_Ride_Model>() {
                                @Override
                                public void success(Create_Ride_Model otp_valid_model, Response response) {

                                    Utils.hideProgress1();
                                    Bundle args = new Bundle();
                                    args.putString("driver_id", String.valueOf(driver_id));
                                    args.putString("ride_id", String.valueOf(ride_id));
                                    navController.navigate(R.id.result_fragment_to_accepted_fragment ,args);

                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                    Utils.hideProgress1();
                                }
                            });
                } else {
                    Toast.makeText(mCtx, "Please connect to an internet", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

*//*

        }
*/
    }


    public String getJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("driver_id",  driver_id);
            obj.put("ride_id", ride_id);

        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return obj.toString();
    }

}
