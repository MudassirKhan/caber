package com.muverity.caber.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.api.AppConst;
import com.muverity.caber.model.DriverNotification_Model;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Driver_Notification_Adapter extends RecyclerView.Adapter<Driver_Notification_Adapter.ViewHolder>{

    ArrayList<DriverNotification_Model> list;;
    private Context mCtx;

    public Driver_Notification_Adapter(Context mCtx ,   ArrayList<DriverNotification_Model> listdata) {
        this.mCtx = mCtx;
        this.list = listdata;
    }
    @NonNull
    @Override
    public Driver_Notification_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.row_driver_notification, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull Driver_Notification_Adapter.ViewHolder holder, final int position) {

        if(list.get(position).getPassengerDetails().getProfilePic()!= null){

            if(!list.get(position).getPassengerDetails().getProfilePic().equals("")){{
                Picasso.with(mCtx)
                        .load(list.get(position).getPassengerDetails().getProfilePic())
                        .placeholder(R.drawable.default_profile)
                        .into(holder.img_profile);
            }
            }
        }

        holder.tv_name.setText(list.get(position).getPassengerDetails().getFirstName());
        holder.tv_budget.setText("$" +""+list.get(position).getRideDetails().getBudget());
        holder.tv_ride_location.setText(list.get(position).getRideDetails().getPickupName() +  " "+"to" + " "+ list.get(position).getRideDetails().getDropName() );
        holder.tv_time.setText("before 5 mintus");

        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final NavController navController = Navigation.findNavController((Activity) mCtx, R.id.my_nav_host_fragment);

                Bundle args = new Bundle();

                args.putInt("passenger_det_id", list.get(position).getPassengerDetails().getId());
                args.putString("passenger_det_name", list.get(position).getPassengerDetails().getFirstName());
                args.putString("passenger_det_profile", list.get(position).getPassengerDetails().getProfilePic());
                args.putString("passenger_det_num", list.get(position).getPassengerDetails().getPhoneNumber());

                args.putString("ride_det_pickname", list.get(position).getRideDetails().getPickupName());
                args.putString("ride_det_picklat", list.get(position).getRideDetails().getPickupLatitude());
                args.putString("ride_det_picklong", list.get(position).getRideDetails().getPickupLongitude());
                args.putString("ride_det_budjet", list.get(position).getRideDetails().getBudget());
                args.putString("ride_det_dropname", list.get(position).getRideDetails().getDropName());
                args.putString("ride_det_droplat", list.get(position).getRideDetails().getDropLatitude());
                args.putString("ride_det_droplong", list.get(position).getRideDetails().getDropLongitude());
                args.putInt("ride_det_id", list.get(position).getRideDetails().getId());
                args.putString("ride_det_type", list.get(position).getRideDetails().getRideType());
                args.putInt("ride_det_passengerid", list.get(position).getRideDetails().getPassenger());
                args.putString("ride_det_status", list.get(position).getRideDetails().getStatus());
                args.putString("ride_det_period", list.get(position).getRideDetails().getCreatedAt());

                args.putString("notification_period", list.get(position).getCreatedAt());
                args.putString("notification_text", list.get(position).getNotificationText());
                args.putInt("notification_id", list.get(position).getId());


                navController.navigate(R.id.driver_notification_to_view_notification_fragment , args);

            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_profile;
        TextView tv_name , tv_car_name , tv_view , tv_ride_location , tv_budget , tv_time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_profile = itemView.findViewById(R.id.img_profile);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_view = itemView.findViewById(R.id.tv_view);
            tv_ride_location = itemView.findViewById(R.id.tv_ride_location);
            tv_budget = itemView.findViewById(R.id.tv_budget);
            tv_time = itemView.findViewById(R.id.tv_time);

        }


    }
}
