package com.muverity.caber.adapter;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.notification.Notification_Model;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Notification_Adapter extends RecyclerView.Adapter<Notification_Adapter.ViewHolder>{

    ArrayList<Notification_Model> list;
    private Context mCtx;


    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public Notification_Adapter(Context mCtx ,ArrayList<Notification_Model> listdata) {
        this.mCtx = mCtx;
        this.list = listdata;
    }

    @NonNull
    @Override
    public Notification_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.row_notification, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull Notification_Adapter.ViewHolder holder, int position) {


        holder.tv_name.setText(list.get(position).getDriverDetails().getFirstName());

      //  holder.tv_time.setText("Less than "+ 5 + "minuts ago");

        Picasso.with(mCtx)
                .load(list.get(position).getDriverDetails().getProfilePic())
                .placeholder(R.drawable.default_profile)
                .into(holder.img_profile);

        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date past = format.parse(list.get(position).getCreatedAt());
            Date now = new Date();

            final long diff = now.getTime() - past.getTime();
            if (diff < MINUTE_MILLIS) {
                holder.tv_time.setText("just now");
            } else if (diff < 2 * MINUTE_MILLIS) {
                holder.tv_time.setText("Less than " +" "+ 1 + " " +"minute ago");
            } else if (diff < 50 * MINUTE_MILLIS) {
                holder.tv_time.setText("Less than " +" "+  diff / MINUTE_MILLIS + " " +"minute ago");
            } else if (diff < 90 * MINUTE_MILLIS) {
                holder.tv_time.setText(1+ " " + "hour ago");
            } else if (diff < 24 * HOUR_MILLIS) {
                holder.tv_time.setText(diff / HOUR_MILLIS + " " + "hour ago");
            } else if (diff < 48 * HOUR_MILLIS) {
                holder.tv_time.setText("yesterday");
            } else {
                holder.tv_time.setText( diff / DAY_MILLIS + " days ago");
            }

        }
        catch (Exception j){
            j.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_profile;
        TextView tv_name , tv_time ;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_profile = itemView.findViewById(R.id.img_profile);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {


        }

    }


}
