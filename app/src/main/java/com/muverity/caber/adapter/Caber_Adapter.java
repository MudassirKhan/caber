package com.muverity.caber.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.model.Cab_Model;
import com.muverity.caber.R;

public class Caber_Adapter extends RecyclerView.Adapter<Caber_Adapter.ViewHolder>{

    private Cab_Model[] listdata;
    int selectedPosition=-1;
    PreferenceUtils pref;
    Activity mContext;


    public Caber_Adapter(Activity mContext ,Cab_Model[] listdata) {
        this.listdata = listdata;
        this.mContext = mContext;
        pref = new PreferenceUtils(mContext);
    }


    @NonNull
    @Override
    public Caber_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.row_cab, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull Caber_Adapter.ViewHolder holder, final int position) {

        holder.tv_car_name.setText(listdata[position].getCar_name());
        holder.img_car.setImageResource(listdata[position].getCar_image());

        if(selectedPosition==position)
            holder.itemView.setBackgroundColor(Color.parseColor("#C0C0C0"));
        else
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition= position;
                pref.setcartype(listdata[position].getCar_name());
                notifyDataSetChanged();

            }
        });

    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_car;
        TextView tv_car_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_car = itemView.findViewById(R.id.img_car);
            tv_car_name = itemView.findViewById(R.id.tv_car_name);

        }
    }
}
