package com.muverity.caber;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class AutonPlace_Activity extends AppCompatActivity implements OnMapReadyCallback , LocationListener , GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    int AUTOCOMPLETE_REQUEST_CODE = 1;
    private GoogleMap mMap;
    private MapView mMapView;
    Marker mCurrLocationMarker;
    Location mLastLocation;
    double lat = 0.0, lng = 0.0;
    Intent intent;
    Button btnSubmit;
    String address  = "";

    double glatitude = 22.5726;
    double glongitutde = 88.3639;
    String  str_marker= "Marker in Sydney";

    public LocationManager locationManager;
    protected LocationListener locationListener;
    protected Context context;
    protected String latitude,longitude;
    protected boolean gps_enabled,network_enabled;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.autoplace_activity);

        btnSubmit = findViewById(R.id.btnSubmit);

        String apiKey = "AIzaSyCAOOaU0VYh5pX__eTrS1QYpTlaW1Ixn3A";
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            intent = new Intent(getApplicationContext(), AccurateLocationService.class);
            AccurateLocationService.enqueueWork(getApplicationContext(), intent);
        } else {
            showGPSDisabledAlertToUser();
        }

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("TAG", "Place: " + place.getName() + ", " + place.getId());
                Log.i("TAG", "Place: " + place.getLatLng() + ", " + place.getId());
                address = place.getName();

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(AutonPlace_Activity.this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocationName(address, 1);
                     glatitude    = addresses.get(0).getLatitude();
                     glongitutde  = addresses.get(0).getLongitude();
                     str_marker = address;

                    mMapView.onCreate(savedInstanceState);
                    mMapView.getMapAsync(AutonPlace_Activity.this);
                    mMapView.onResume();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                mMap.clear();

                Log.i("TAG", "Place: " +place.getLatLng()  + ", " + place.getId());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("TAG", "An error occurred: " + status);
            }
        });


    /*    List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);*/

        mMapView = findViewById(R.id.mapView);

        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
        mMapView.onResume(); // needed to get the map to display immediately

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("address", "" + address);
               // intent.putExtra("lng", "" + lng);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        this.mMap = googleMap;
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title(str_marker));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        addMarker(glatitude, glongitutde);

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = this.mMap.addMarker(markerOptions);

        //move map camera
        this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));


    }

    private void addMarker(double lat, double lng) {
        this.mMap.clear();
        this.mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10));
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (lat == 0.0) {
                updateUI(intent);
            }
        }
    };

    private void updateUI(Intent intent) {
        String strlat = intent.getStringExtra("lat");
        String strlng = intent.getStringExtra("lng");

        lat = Double.parseDouble(strlat);
        lng = Double.parseDouble(strlng);
        mMap.clear();
        LatLng sydney = new LatLng(Double.parseDouble(strlat), Double.parseDouble(strlng));
        mMap.addMarker(new MarkerOptions().position(sydney).title(""));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12.0f));

    }

    @Override
    public void onResume() {
        super.onResume();
        intent = new Intent(getApplicationContext(), AccurateLocationService.class);
        AccurateLocationService.enqueueWork(getApplicationContext(), intent);
        startService(intent);
        registerReceiver(broadcastReceiver, new IntentFilter(AccurateLocationService.BROADCAST_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        stopService(intent);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
