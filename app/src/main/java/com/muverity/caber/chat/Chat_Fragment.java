package com.muverity.caber.chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.adapter.Chat_Adapter;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.ContactListModel;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Chat_Fragment extends Fragment {

    ImageView img_notification;
    ImageView iv_back;
    PreferenceUtils pref;
    Chat_Adapter language_adapter;
    RecyclerView rv_profile_cars ;
    ArrayList<ContactListModel> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        rv_profile_cars = (RecyclerView)view. findViewById(R.id.rv_chats);
        ImageView img_notification = view. findViewById(R.id.img_notification);
        iv_back = view.findViewById(R.id.iv_back);
        pref = new PreferenceUtils(getActivity());

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        rv_profile_cars.setHasFixedSize(true);
        rv_profile_cars.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.VERTICAL, false));


        img_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getrole().equals("1")){
                    navController.navigate(R.id.chat_fragment_to_notification_fragment);
                }else {
                    navController.navigate(R.id.chat_fragment_to_driver_notification);
                }

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        Callwschat();
    }

    private void Callwschat() {

        try {
            if (Utils.isNetworkAvailable(getActivity())) {
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.contact_list(
                        "Token "+ pref.getauthtoke(),
                        new Callback<ArrayList<ContactListModel>>() {
                            @Override
                            public void success(ArrayList<ContactListModel> modelLeaves, Response response) {

                                Utils.hideProgress1();
                                list = modelLeaves;
                                language_adapter = new Chat_Adapter(getActivity(), list);
                                rv_profile_cars.setAdapter(language_adapter);
                                language_adapter.notifyDataSetChanged();

                            }
                            @Override
                            public void failure(RetrofitError error) {
                                Utils.hideProgress1();
                            }

                        });
            } else {
                Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}



