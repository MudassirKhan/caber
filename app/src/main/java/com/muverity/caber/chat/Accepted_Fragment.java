package com.muverity.caber.chat;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.muverity.caber.R;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Accepted_Fragment extends Fragment implements OnMapReadyCallback, LocationListener {


    ImageView iv_back , img_call;
    ImageView img_profile ;
    private GoogleMap googleMap;
    SupportMapFragment mapFragment;
    Marker mCurrLocationMarker;
    Location mLastLocation;
    TextView tv_name , tv_source , tv_destination;
    int driver_id ,total_rides ,ride_id,passenger_id;
    double total_rating;
    String name ,budget , pick , drop ,pick_lat,pick_long,drop_lat,drop_lng ,ride_type,status,profile ,number;
    ArrayList<String> list ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.accepted_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        changeStatusBarColor(getResources().getColor(R.color.white));
        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);


        driver_id  = getArguments().getInt("driver_id");
        total_rides  = getArguments().getInt("total_rides");
        number  = getArguments().getString("number");
        Log.e("TAG" , "TEST_NUM" + getArguments().getString("number"));
        ride_id  = getArguments().getInt("ride_id");
        passenger_id  = getArguments().getInt("passenger_id");
        total_rating  = getArguments().getDouble("total_rating");
        list= (ArrayList<String>)getArguments().getSerializable("list");
        name  = getArguments().getString("name");
        budget  = getArguments().getString("budget");
        pick  = getArguments().getString("pick");
        drop  = getArguments().getString("drop");
        pick_lat  = getArguments().getString("pick_lat");
        pick_long  = getArguments().getString("pick_long");
        drop_lat  = getArguments().getString("drop_lat");
        drop_lng  = getArguments().getString("drop_lng");
        ride_type  = getArguments().getString("ride_type");
        status  = getArguments().getString("status");
        profile  = getArguments().getString("profile");

        img_call = view.findViewById(R.id.img_call);
        iv_back = view.findViewById(R.id.iv_back);
        img_profile = view.findViewById(R.id.img_profile);

        tv_name = view.findViewById(R.id.tv_name);
        tv_source = view.findViewById(R.id.tv_source);
        tv_destination = view.findViewById(R.id.tv_destination);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        if (profile != null) {
            if (!profile.equals("")) {
                Picasso.with(getActivity()).load(profile).placeholder(R.drawable.default_profile).into(img_profile);
            }
        }

        if (name!= null) {
            if (!name.equals("")) {
                tv_name.setText(name );
            }
        }

        if (pick!= null) {
            if (!pick.equals("")) {
                tv_source.setText(pick);
            }
        }

        if (drop!= null) {
            if (!drop.equals("")) {
                tv_destination.setText(drop);
            }
        }



        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        img_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDailogue();
            }
        });

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle args = new Bundle();

                args.putInt("driver_id", driver_id);
                args.putInt("total_rides", total_rides);
                args.putString("name",name);
                args.putString("profile", profile);
                args.putString("total_rating", String.valueOf(total_rating));
                args.putSerializable("list", list);

                navController.navigate(R.id.accepted_fragment_to_caber_profile , args);

            }
        });

        img_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDailogue();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

    }

    private void openDailogue() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.custom_call);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        EditText edt_phone_number = (EditText) dialog.findViewById(R.id.edt_phone_number);

        Log.e("TAG" , "TEST_NUM" + number);
        edt_phone_number.setText(number);

        final String number = edt_phone_number.getText().toString();

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                callPhoneNumber(number);
              /*
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData( Uri.parse("tel:" + edt_phone_number.getText().toString()));
                startActivity(callIntent);*/
            }
        });

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialog.show();
    }

    public void callPhoneNumber(String number) {
        try
        {
            if(Build.VERSION.SDK_INT > 22)
            {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CALL_PHONE}, 101);
                    return;
                }

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                startActivity(callIntent);

            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" +number));
                startActivity(callIntent);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void addMarker(double lat, double lng) {
        this.googleMap.clear();
        this.googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10));
    }

    private void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        addMarker(22.5726, 88.3639);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = this.googleMap.addMarker(markerOptions);

        //move map camera
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
    }

}
