package com.muverity.caber.chat;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.muverity.caber.Manifest;
import com.muverity.caber.R;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

public class View_Notification_Fragment extends Fragment implements OnMapReadyCallback, LocationListener {

   public ImageView img_profile ;
   public TextView tv_name ,tv_pickname,tv_dropname;

    public  ImageView iv_back , img_call;
    public GoogleMap googleMap;
    public  SupportMapFragment mapFragment;
    public Marker mCurrLocationMarker;
    public Location mLastLocation;

   public String  passenger_det_name,passenger_det_profile,passenger_det_num , ride_det_pickname , ride_det_picklat, ride_det_picklong ,
            ride_det_budjet , ride_det_dropname,ride_det_droplat,ride_det_droplong ,ride_det_type,ride_det_status,
            ride_det_period,notification_period, notification_text;

    public  int passenger_det_id ,ride_det_id , ride_det_passengerid , notification_id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_driver_notification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        changeStatusBarColor(getResources().getColor(R.color.white));


        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        img_call = view.findViewById(R.id.img_call);
        iv_back = view.findViewById(R.id.iv_back);

        img_profile = view.findViewById(R.id.img_profile);
        tv_name = view.findViewById(R.id.tv_name);
        tv_pickname = view.findViewById(R.id.tv_pickname);
        tv_dropname = view.findViewById(R.id.tv_dropname);

        passenger_det_id  = getArguments().getInt("passenger_det_id");
        passenger_det_name  = getArguments().getString("passenger_det_name");
        passenger_det_profile  = getArguments().getString("passenger_det_profile");
        passenger_det_num  = getArguments().getString("passenger_det_num");
        ride_det_pickname  = getArguments().getString("ride_det_pickname");
        ride_det_picklat  = getArguments().getString("ride_det_picklat");
        ride_det_picklong  = getArguments().getString("ride_det_picklong");
        ride_det_budjet  = getArguments().getString("ride_det_budjet");
        ride_det_dropname  = getArguments().getString("ride_det_dropname");
        ride_det_droplat  = getArguments().getString("ride_det_droplat");
        ride_det_droplong  = getArguments().getString("ride_det_droplong");
        ride_det_id  = getArguments().getInt("ride_det_id");
        ride_det_type  = getArguments().getString("ride_det_type");
        ride_det_passengerid  = getArguments().getInt("ride_det_passengerid");
        ride_det_status  = getArguments().getString("ride_det_status");
        ride_det_period  = getArguments().getString("ride_det_period");
        notification_period  = getArguments().getString("notification_period");
        notification_text  = getArguments().getString("notification_text");
        notification_id  = getArguments().getInt("notification_id");

        Picasso.with(getActivity()).load(passenger_det_profile).placeholder(R.drawable.default_profile).into(img_profile);
        tv_name.setText(passenger_det_name);
        tv_pickname.setText(ride_det_pickname);
        tv_dropname.setText(ride_det_dropname);

        if(passenger_det_profile != null){
            if(!passenger_det_profile.equals("")){
                Picasso.with(getActivity()).load(passenger_det_profile).placeholder(R.drawable.default_profile).into(img_profile);
            }
        }

        if(passenger_det_name != null){
            if(!passenger_det_name.equals("")){
                tv_name.setText(passenger_det_name);
            }
        }



        if(ride_det_pickname != null){
            if(!ride_det_pickname.equals("")){
                tv_pickname.setText(ride_det_pickname);
            }
        }



        if(ride_det_dropname != null){
            if(!ride_det_dropname.equals("")){
                tv_dropname.setText(ride_det_dropname);
            }
        }



        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // navController.navigate(R.id.accepted_fragment_to_caber_profile);
            }
        });

        img_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDailogue();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

    }

    private void openDailogue() {

        final Dialog dialog = new Dialog(getActivity());
       // dialog.setContentView(R.layout.askprice_dialogue);
        dialog.setContentView(R.layout.custom_call);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        final EditText edt_phone_number = (EditText) dialog.findViewById(R.id.edt_phone_number);
        edt_phone_number.setText(passenger_det_num);

        final String number = edt_phone_number.getText().toString();

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                callPhoneNumber(number);
              /*
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData( Uri.parse("tel:" + edt_phone_number.getText().toString()));
                startActivity(callIntent);*/
            }
        });

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    public void callPhoneNumber(String number) {
        try
        {
            if(Build.VERSION.SDK_INT > 22)
            {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling

                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CALL_PHONE}, 101);

                    return;
                }

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                startActivity(callIntent);

            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" +number));
                startActivity(callIntent);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void addMarker(double lat, double lng) {
        this.googleMap.clear();
        this.googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10));
    }

    private void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        addMarker(22.5726, 88.3639);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = this.googleMap.addMarker(markerOptions);

        //move map camera
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
    }

}
