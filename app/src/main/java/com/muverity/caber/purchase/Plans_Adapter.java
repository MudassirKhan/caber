package com.muverity.caber.purchase;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.MyApplication;
import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.Create_Ride_Model;
import com.muverity.caber.model.PlansModel;
import com.muverity.caber.model.PlansPurchaseModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class Plans_Adapter extends RecyclerView.Adapter<Plans_Adapter.ViewHolder>{

    ArrayList<PlansModel> list;
    private Context mCtx;
    int plans_id ;
    PreferenceUtils pref;


    public Plans_Adapter(Context mCtx , ArrayList<PlansModel> listdata) {
        this.mCtx = mCtx;
        this.list = listdata;
        pref = new PreferenceUtils(mCtx);
    }

    @NonNull
    @Override
    public Plans_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.row_plans, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull Plans_Adapter.ViewHolder holder, final int position) {

        holder.tv_price.setText("$" + list.get(position).getCost());
        holder.tv_period.setText(list.get(position).getName());

        holder.img_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                plans_id = list.get(position).getId();

                String rawdata = getJson();
                TypedInput in = null;
                try {
                    in = new TypedByteArray("application/json", rawdata.getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                if (Utils.isNetworkAvailable(mCtx)) {
                    Utils.showProgress1("Please wait", mCtx);
                    CaberWSapi api = APIService.createService(CaberWSapi.class);
                    api.purchase_plans(
                            "Token "+ pref.getauthtoke(),
                            in,
                            new Callback<PlansPurchaseModel>() {
                                @Override
                                public void success(PlansPurchaseModel otp_valid_model, Response response) {

                                    Utils.hideProgress1();

                                    Toast.makeText(mCtx, "Successfully purchased ...", Toast.LENGTH_SHORT).show();

                                    pref.settransid(otp_valid_model.getPlanDetails().getId());

                                    final NavController navController = Navigation.findNavController((Activity) mCtx, R.id.my_nav_host_fragment);

                                    if(pref.getprofilestatus().equals("1") || pref.getcarstatus().equals("1")){

                                        navController.navigate(R.id.purchase_fragment_to_order_fragment);
                                    }else {
                                        navController.navigate(R.id.purchase_fragment_to_upload_pics_fragment);
                                    }

                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Log.e("TAG" , "TESTING_ERROR" + error);
                                    Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                    Utils.hideProgress1();
                                }
                            });
                } else {
                    Toast.makeText(mCtx, "Please connect to an internet", Toast.LENGTH_SHORT).show();
                }



                /*final NavController navController = Navigation.findNavController((Activity) mCtx, R.id.my_nav_host_fragment);
                navController.navigate(R.id.purchase_fragment_to_purchasecard_fragment );*/

            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_purchase ;
        TextView tv_price , tv_period , txt1 , txt2 , txt3 , txt4 , txt5;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_purchase = itemView.findViewById(R.id.img_purchase);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_period = itemView.findViewById(R.id.tv_period);
            txt1 = itemView.findViewById(R.id.txt1);
            txt2 = itemView.findViewById(R.id.txt2);
            txt3 = itemView.findViewById(R.id.txt3);
            txt4 = itemView.findViewById(R.id.txt4);
            txt5 = itemView.findViewById(R.id.txt5);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {


        }

    }

    public String getJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("plan_id", plans_id);
            obj.put("transaction_id", "NA");

        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return obj.toString();
    }
}
