package com.muverity.caber.purchase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.PlansModel;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class purchase_fragment extends Fragment {

    ImageView img_purchase , iv_back;

    Plans_Adapter language_adapter;
    RecyclerView rv_plans ;
    ArrayList<PlansModel> list = new ArrayList<>();
    ArrayList<PlansModel> list2 = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.purchase_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);



        iv_back = view.findViewById(R.id.iv_back);
      //  img_purchase = view.findViewById(R.id.img_purchase);
        rv_plans = (RecyclerView)view. findViewById(R.id.rv_plans);

        rv_plans.setHasFixedSize(true);
        rv_plans.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL, false));

       /* img_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.purchase_fragment_to_purchasecard_fragment);
            }
        });*/

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        callWSplans();
    }

    private void callWSplans() {
        try {
            if (Utils.isNetworkAvailable(getActivity())) {
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.plans(
                        new Callback<ArrayList<PlansModel>>() {
                            @Override
                            public void success(ArrayList<PlansModel> modelLeaves, Response response) {

                                Utils.hideProgress1();
                                list = modelLeaves;
                                list2 = modelLeaves;
                              //  list.addAll(list2);
                                language_adapter = new Plans_Adapter(getActivity(), list);
                                rv_plans.setAdapter(language_adapter);
                                language_adapter.notifyDataSetChanged();
                            }
                            @Override
                            public void failure(RetrofitError error) {
                                Utils.hideProgress1();
                            }

                        });
            } else {
                Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
