package com.muverity.caber.common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.muverity.caber.R;
import com.muverity.caber.api.PreferenceUtils;
import com.hbb20.CountryCodePicker;

public class Splash3_fragment extends Fragment {

    TextView btn_signin , tv_next ;
    PreferenceUtils pref;
    EditText et_mobile ;
    NavController navController;
    CountryCodePicker ccp;
    String   countryNumber , countryCode ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.splash3_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_signin = view.findViewById(R.id.btn_signin);
        tv_next = view.findViewById(R.id.tv_next);
        et_mobile = view.findViewById(R.id.et_mobile);
        ccp = view.findViewById(R.id.ccp);
        ccp.getSelectedCountryCodeAsInt();

        navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        pref = new PreferenceUtils(getActivity());

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobile = et_mobile.getText().toString().trim();

                if(mobile.isEmpty() || mobile.length() < 10){
                    et_mobile.setError("Enter a valid mobile");
                    et_mobile.requestFocus();
                    return;
                }

              String phone_number =   "+"+countryCode+et_mobile.getText().toString();
                Bundle args = new Bundle();
                args.putString("phone_number", phone_number);
                navController.navigate(R.id.splash3_fragment_to_otp_fragment , args);

              /*  sendotp();*/

            }
        });


        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.splash3_fragment_to_registration_fragment);
            }
        });

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryNumber = ccp.getSelectedCountryNameCode();
                countryCode   = ccp.getSelectedCountryCode();
            }

        });


    }

  /*  private void send_number(String phone_number) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone_number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);

    }*/

   /* private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                editTextCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };*/

  /*  private void sendotp() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if(Utils.isValidPhone(et_mobile.getText().toString()) && et_mobile.length() != 0  ){
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.send_otp(
                        "+91"+et_mobile.getText().toString(),
                        new Callback<Splash3_Send_Opt_Model >() {
                            @Override
                            public void success(Splash3_Send_Opt_Model splash3_send_opt_model, Response response) {

                                Utils.hideProgress1();

                                String str_otp =  splash3_send_opt_model.getOtp();
                                String str_pnumber =  splash3_send_opt_model.getPhone_number();
                                String str_pk =  splash3_send_opt_model.getPk();

                                pref.setnumber(str_pnumber);
                                pref.setotp(str_otp);
                                pref.setpk(str_pk);

                                navController.navigate(R.id.splash3_fragment_to_otp_fragment);

                            }

                            @Override
                            public void failure(RetrofitError error) {

                                Toast.makeText(getActivity(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }
                        });

            }else {
                Toast.makeText(getActivity(), "Invalid email id", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }

    }*/

}
