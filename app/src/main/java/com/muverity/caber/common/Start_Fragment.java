package com.muverity.caber.common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.muverity.caber.R;
import com.muverity.caber.api.PreferenceUtils;

public class Start_Fragment extends Fragment {

    Button btn_home;
    ImageView iv_back;
    String key;
    PreferenceUtils pref;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
        key = getArguments().getString("key");
        btn_home = view.findViewById(R.id.btn_home);
        iv_back = view.findViewById(R.id.iv_back);
        pref = new PreferenceUtils(getActivity());

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(key.equals("driver")){


                    if(! pref.gettransid().equals("") || pref.gettransid().equals("0")){

                        if(pref.getprofilestatus().equals("1") || pref.getcarstatus().equals("1")){
                            navController.navigate(R.id.start_fragment_to_order_fragment);
                        }else {
                            navController.navigate(R.id.start_fragment_to_upload_pics_fragment);
                        }

                    }else{

                        navController.navigate(R.id.start_fragment_to_purchase_fragment);
                    }



                }else {
                    navController.navigate(R.id.start_fragment_to_home_fragment);
                }

            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


    }
}
