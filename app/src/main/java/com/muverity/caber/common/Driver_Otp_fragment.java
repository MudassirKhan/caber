package com.muverity.caber.common;

import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.muverity.caber.MyApplication;
import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.FilePath;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.customview.PinEntryEditText;
import com.muverity.caber.model.Otp_Valid_Model;
import com.google.firebase.FirebaseApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class Driver_Otp_fragment extends Fragment {

    PreferenceUtils pref;
    Button btn_submit;
    TextView tv_edit;
    EditText et_email;
    String str_token , deviceId;
    NavController navController ;
    String str_otp;
    String str_pk;
    String phone_number;
    String driver_name;
    String car_model;
    String car_ownership;
    String str_cartype;
    Uri str_licen;
    Uri str_ownership;
    String token;
    ImageView iv_back;
    PinEntryEditText pinEntry;

    FirebaseAuth mAuth;
    String mVerificationId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.driver_otp_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FirebaseApp.initializeApp(MyApplication.getAppContext());

        navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
        pinEntry = (PinEntryEditText) view.findViewById(R.id.txt_pin_entry);

        Utils.showProgress1("Please wait", getActivity());

       /* str_otp = getArguments().getString("str_otp");
        str_pk = getArguments().getString("str_pk");*/

        phone_number = getArguments().getString("phone_number");
        driver_name = getArguments().getString("driver_name");
        car_model = getArguments().getString("car_model");
        car_ownership = getArguments().getString("car_ownership");
        str_cartype = getArguments().getString("str_cartype");
        str_licen = Uri.parse(getArguments().getString("str_licen"));
        str_ownership = Uri.parse(getArguments().getString("str_ownership"));

        mAuth = FirebaseAuth.getInstance();
        sendVerificationCode(phone_number);

        btn_submit = view.findViewById(R.id.btn_submit);
        tv_edit = view.findViewById(R.id.tv_edit);
        et_email = view.findViewById(R.id.et_email);
        iv_back = view.findViewById(R.id.iv_back);
        pref = new PreferenceUtils(getActivity());

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        token = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        pref.settoken(token);

       // str_token = FirebaseInstanceId.getInstance().getToken();
        deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        pref.settoken(deviceId);

        et_email.setEnabled(false);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.emailValidator(et_email.getText().toString()) && et_email.length() != 0){

                    String code = pinEntry.getText().toString().trim();
                    if (code.isEmpty() || code.length() < 6) {
                        pinEntry.setError("Enter valid code");
                        pinEntry.requestFocus();
                        return;
                    }

                    verifyVerificationCode(code);

                }else {
                    Toast.makeText(getActivity(), "Invalid email id", Toast.LENGTH_SHORT).show();
                }

               // navController.navigate(R.id.driver_otp_profile_to_purchase_fragment);

            }
        });

        final TextView txt_count=view.findViewById(R.id.txt_count);

        new CountDownTimer(180000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                txt_count.setText(""+String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                txt_count.setText("done!");
            }

        }.start();

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_email.setEnabled(true);
                et_email.setFocusable(true);
                et_email.requestFocus();
                et_email.setCursorVisible(true);

            }
        });

       /* pinEntry.setText(pref.getotp());

        pinEntry.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().equals("1234")) {

                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinEntry.getWindowToken(), 0);

                } else if (s.length() == "1234".length()) {

                    Toast.makeText(getActivity(), "Incorrect", Toast.LENGTH_SHORT).show();
                    pinEntry.setText(null);

                }
            }
        });*/

       /* pinEntry.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 4){
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinEntry.getWindowToken(), 0);
                }
            }
        });*/

    }

   /* private void validateOtp() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if(Utils.emailValidator(et_email.getText().toString()) && et_email.length() != 0){
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.validate_otp(
                        pref.getpk(),
                        pref.getotp(),
                        et_email.getText().toString(),
                        deviceId,
                        "android",
                        "2",
                        new Callback<Otp_Valid_Model>() {
                            @Override
                            public void success(Otp_Valid_Model otp_valid_model, Response response) {
                                Utils.hideProgress1();

                                pref.setfirstname(otp_valid_model.getFirst_name());
                                pref.setlastname(otp_valid_model.getLast_name());
                                pref.setemail(otp_valid_model.getEmail());
                                pref.setrole(otp_valid_model.getRole());
                                pref.setcar_model(otp_valid_model.getCar_model());
                                pref.setcar_ownership_doc(otp_valid_model.getCar_ownership_doc());
                                pref.setcountry_code(otp_valid_model.getCountry_code());
                                pref.setdriver_licence(otp_valid_model.getDriver_licence());
                                pref.setprofile_pic(otp_valid_model.getProfile_pic());
                                pref.setauthtoke(otp_valid_model.getAuth_token());
                                pref.setlogin("1");

                                navController.navigate(R.id.driver_otp_profile_to_purchase_fragment);

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }
                        });

            }else {

                Toast.makeText(getActivity(), "Invalid mobile number", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }
    }*/

    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                Utils.hideProgress1();
                pinEntry.setText(code);
                verifyVerificationCode(code);
            }else {
                Toast.makeText(getActivity(),code, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            String str_email = et_email.getText().toString();
                            String[] result = str_email.split("@");
                            str_email = result[0];

                            TypedFile uploadlicens = null;
                            if (str_licen != null) {
                                uploadlicens = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), str_licen)));
                            }

                            TypedFile oploadownership = null;
                            if (str_ownership != null) {
                                oploadownership = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), str_ownership)));
                            }

                            if (Utils.isNetworkAvailable(getActivity())) {
                                Utils.showProgress1("Please wait", getActivity());
                                CaberWSapi api = APIService.createService(CaberWSapi.class);
                                api.driver_singup_profile(
                                        phone_number,
                                        et_email.getText().toString(),
                                        driver_name,
                                        str_cartype,
                                     2,
                                        car_model,
                                        token,
                                     "android",
                                        new Callback<Otp_Valid_Model>() {
                                            @Override
                                            public void success(Otp_Valid_Model otp_valid_model, Response response) {
                                                Utils.hideProgress1();

                                                if(response.getStatus() == 200){

                                                    pref.setfirstname(otp_valid_model.getFirst_name());
                                                    pref.setlastname(otp_valid_model.getLast_name());
                                                    pref.setemail(otp_valid_model.getEmail());
                                                    pref.setrole(otp_valid_model.getRole());
                                                    pref.setcar_model(otp_valid_model.getCar_model());
                                                    pref.setcar_ownership_doc(otp_valid_model.getCar_ownership_doc());
                                                    pref.setcountry_code(otp_valid_model.getCountry_code());
                                                    pref.setdriver_licence(otp_valid_model.getDriver_licence());
                                                    pref.setprofile_pic(otp_valid_model.getProfile_pic());
                                                    pref.setauthtoke(otp_valid_model.getAuth_token());
                                                    pref.setlogin("1");
                                                    pref.setuserid(otp_valid_model.getId());
                                                    pref.setcartype(otp_valid_model.getCar_type());
                                                    pref.settotal_rating(otp_valid_model.getTotal_rating());
                                                    pref.setcars((ArrayList) otp_valid_model.getCarPic());
                                                    pref.gsettotal_rides(otp_valid_model.getTotal_rides());

                                                    Bundle bundle = new Bundle();
                                                    bundle.putString("key","driver");
                                                    navController.navigate(R.id.driver_otp_profile_to_start_fragment , bundle);

                                                }else {
                                                    Toast.makeText(MyApplication.getAppContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                                }


                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                Log.e("TAG" , "ERRORDEt" + error);
                                                Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                                Utils.hideProgress1();
                                            }
                                        });

                            } else {
                                Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
                            }

                            //verification successful we will start the profile activity
                           /* Intent intent = new Intent(VerifyPhoneActivity.this, ProfileActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);*/

                        } else {

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }


    /*private void singup() {

     *//*   String rawdata = getJson();
        TypedInput in = null;
        try {
            in = new TypedByteArray("application/json", rawdata.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (Utils.isNetworkAvailable(getActivity())) {
            if(Utils.emailValidator(et_email.getText().toString()) && et_email.length() != 0){
                Utils.showProgress1("Please wait", getActivity());


                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.driver_singup_profile(
                        in,
                        new Callback<Otp_Valid_Model>() {
                            @Override
                            public void success(Otp_Valid_Model otp_valid_model, Response response) {
                                Utils.hideProgress1();
                                if(response.getStatus() == 200){

                                    pref.setfirstname(otp_valid_model.getFirst_name());
                                    pref.setlastname(otp_valid_model.getLast_name());
                                    pref.setemail(otp_valid_model.getEmail());
                                    pref.setrole(otp_valid_model.getRole());
                                    pref.setcar_model(otp_valid_model.getCar_model());
                                    pref.setcar_ownership_doc(otp_valid_model.getCar_ownership_doc());
                                    pref.setcountry_code(otp_valid_model.getCountry_code());
                                    pref.setdriver_licence(otp_valid_model.getDriver_licence());
                                    pref.setprofile_pic(otp_valid_model.getProfile_pic());
                                    pref.setauthtoke(otp_valid_model.getAuth_token());
                                    pref.setlogin("1");
                                    pref.setuserid(otp_valid_model.getId());
                                    pref.setcartype(otp_valid_model.getCar_type());
                                    pref.gsettotal_rides(otp_valid_model.getCar_type());
                                    pref.settotal_rating(otp_valid_model.getTotal_rating());
                                    pref.setcars((ArrayList) otp_valid_model.getCarPic());

                                    navController.navigate(R.id.driver_otp_profile_to_purchase_fragment);

                                }else {
                                    Toast.makeText(MyApplication.getAppContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }
                        });

            }else {

                Toast.makeText(getActivity(), "Please enter correct email id", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }
*//*

        TypedFile uploadlicens = null;
        if (str_licen != null) {
            uploadlicens = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), str_licen)));
        }
        TypedFile oploadownership = null;
        if (str_ownership != null) {
            oploadownership = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), str_ownership)));
        }

        if (Utils.isNetworkAvailable(getActivity())) {
            Utils.showProgress1("Please wait", getActivity());
            CaberWSapi api = APIService.createService(CaberWSapi.class);
            api.driver_singup_profile(
                    token,
                    str_pk,
                    str_otp,
                    "android",
                    "2",
                    driver_name,
                    driver_name,
                    car_model,
                    uploadlicens,
                    oploadownership,
                    new Callback<Otp_Valid_Model>() {
                        @Override
                        public void success(Otp_Valid_Model otp_valid_model, Response response) {
                            Utils.hideProgress1();

                            if(response.getStatus() == 200){

                                pref.setfirstname(otp_valid_model.getFirst_name());
                                pref.setlastname(otp_valid_model.getLast_name());
                                pref.setemail(otp_valid_model.getEmail());
                                pref.setrole(otp_valid_model.getRole());
                                pref.setcar_model(otp_valid_model.getCar_model());
                                pref.setcar_ownership_doc(otp_valid_model.getCar_ownership_doc());
                                pref.setcountry_code(otp_valid_model.getCountry_code());
                                pref.setdriver_licence(otp_valid_model.getDriver_licence());
                                pref.setprofile_pic(otp_valid_model.getProfile_pic());
                                pref.setauthtoke(otp_valid_model.getAuth_token());
                                pref.setlogin("1");
                                pref.setuserid(otp_valid_model.getId());
                                pref.setcartype(otp_valid_model.getCar_type());
                                pref.settotal_rating(otp_valid_model.getTotal_rating());
                                pref.setcars((ArrayList) otp_valid_model.getCarPic());
                                pref.gsettotal_rides(otp_valid_model.getTotal_rides());

                                navController.navigate(R.id.driver_otp_profile_to_purchase_fragment);

                            }else {
                                Toast.makeText(MyApplication.getAppContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                            }


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("TAG" , "ERRORDEt" + error);
                            Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                            Utils.hideProgress1();
                        }
                    });

        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }

    }

    public String getJson() {

        TypedFile uploadlicens = null;
        if (str_licen != null) {
            uploadlicens = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), str_licen)));
        }
        TypedFile oploadownership = null;
        if (str_ownership != null) {
            oploadownership = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), str_ownership)));
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("device_token", token);
            obj.put("pk", str_pk);
            obj.put("otp", str_otp);
            obj.put("device_type","android");
            obj.put("role","2");
            obj.put("first_name",driver_name);
            obj.put("last_name","");
            obj.put("car_model",car_model);
            obj.put("driver_licence",uploadlicens);
            obj.put("car_ownership_doc",oploadownership);

        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return obj.toString();
    }*/

}
