package com.muverity.caber.common;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.muverity.caber.MyApplication;
import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.AppConst;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.FilePath;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.model.Splash3_Send_Opt_Model;
import com.hbb20.CountryCodePicker;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Singup_fragment extends Fragment {

    EditText et_mobile , edt_drivername , edt_carmodel , edt_driving_licens , edt_carownership;
    Button btn_submit;
    AutoCompleteTextView edt_cartype ;
    static final String[] cartype = new String[] {
            "Select car",
            "Economy",
            "Prime",
            "XL"
    };

    private int SELECT_LICENS = 1;
    private int SELECT_OWNERSHIP = 2;
    String token;
    private Uri filePath;

    private Uri filePath2;
    PreferenceUtils pref;
    String fileType = AppConst.FILE_TYPE_TEXT, fileExtention = "NULL", fileURL = "NULL", fileDeviceURL = "NULL", messageText = "NULL";
    NavController navController ;
    ImageView iv_back;
    Spinner spcartype;
    String str_cartype ="" ;

    CountryCodePicker ccp;
    String   countryNumber , countryCode = "966" ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.singup_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
        et_mobile = view.findViewById(R.id.et_mobile);
        btn_submit = view.findViewById(R.id.btn_submit);
        edt_drivername = view.findViewById(R.id.edt_drivername);
        edt_carmodel = view.findViewById(R.id.edt_carmodel);
        edt_driving_licens = view.findViewById(R.id.edt_driving_licens);
        edt_carownership = view.findViewById(R.id.edt_carownership);
        pref = new PreferenceUtils(getActivity());
        spcartype = view.findViewById(R.id.spcartype);
        iv_back = view.findViewById(R.id.iv_back);

        ccp = view.findViewById(R.id.ccp);
        ccp.getSelectedCountryCodeAsInt();

        token = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        pref.settoken(token);

        ArrayAdapter<String> bloodAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, cartype);
        bloodAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spcartype.setAdapter(bloodAdapter);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryNumber = ccp.getSelectedCountryNameCode();
                countryCode   = ccp.getSelectedCountryCode();
            }
        });

        spcartype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_cartype = cartype[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobile = et_mobile.getText().toString().trim();

                if(mobile.isEmpty() || mobile.length() < 10){
                    et_mobile.setError("Enter a valid mobile");
                    et_mobile.requestFocus();
                    return;
                }


                if(edt_drivername.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please enter your name", Toast.LENGTH_SHORT).show();
                }else  if(edt_carmodel.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please enter car model", Toast.LENGTH_SHORT).show();
                }else  if(et_mobile.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please enter mobile number", Toast.LENGTH_SHORT).show();
                } else  if(str_cartype.equals("Select car")){
                    Toast.makeText(MyApplication.getAppContext(), "Please select car type", Toast.LENGTH_SHORT).show();
                }else  if(edt_driving_licens.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please attach driving licens document", Toast.LENGTH_SHORT).show();
                }else  if(edt_carownership.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please attach car ownership document", Toast.LENGTH_SHORT).show();
                }else {

                    String phone_number ="+"+countryCode+et_mobile.getText().toString();
                    Bundle args = new Bundle();
                    args.putString("phone_number",phone_number);
                    args.putString("driver_name", edt_drivername.getText().toString());
                    args.putString("car_model", edt_carmodel.getText().toString());
                    args.putString("car_ownership",edt_carownership.getText().toString());
                    args.putString("str_cartype",str_cartype);
                    args.putString("str_licen", String.valueOf(getFilePath()));
                    args.putString("str_ownership", String.valueOf(getFilePath2()));

                    Log.e("TAG" , "Numbertest "+phone_number);
                    navController.navigate(R.id.registration_fragment_to_driver_otp_profile , args);




                }

             /*   if(edt_drivername.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please enter first name", Toast.LENGTH_SHORT).show();
                }else  if(edt_carmodel.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please enter last name", Toast.LENGTH_SHORT).show();
                }else  if(edt_carownership.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please attach car ownership document", Toast.LENGTH_SHORT).show();
                }else  if(str_cartype.equals("Select car")){
                    Toast.makeText(MyApplication.getAppContext(), "Please select car type", Toast.LENGTH_SHORT).show();
                }else  if(et_mobile.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please enter mobile number", Toast.LENGTH_SHORT).show();
                }else  if(edt_driving_licens.getText().toString().equals("")){
                    Toast.makeText(MyApplication.getAppContext(), "Please attach driving licens document", Toast.LENGTH_SHORT).show();
                }else {

                    singup();

                   // navController.navigate(R.id.registration_fragment_to_otp_fragment);
                }*/

            }
        });

        edt_driving_licens.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_LICENS);

            }
        });

        edt_carownership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_OWNERSHIP);

            }
        });


    }

   /* private void singup() {

        TypedFile uploadlicens = null;
        if (getFilePath() != null) {
            uploadlicens = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), getFilePath())));
        }
        TypedFile oploadownership = null;
        if (getFilePath2() != null) {
            oploadownership = new TypedFile("multipart/form-data", new File(FilePath.getPath(getActivity(), getFilePath2())));
        }

        if (Utils.isNetworkAvailable(getActivity())) {
                Utils.showProgress1("Please wait", getActivity());
                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.driver_singup_profile(
                        token,
                        "116",
                        "1046",
                        "android",
                        "2",
                        edt_drivername.getText().toString(),
                        edt_drivername.getText().toString(),
                        str_cartype,
                        uploadlicens,
                        oploadownership,
                        new Callback<Otp_Valid_Model>() {
                            @Override
                            public void success(Otp_Valid_Model otp_valid_model, Response response) {
                                Utils.hideProgress1();

                                navController.navigate(R.id.otp_fragment_to_start_fragment);

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }
                        });



        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }



    }*/

    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }

    public Uri getFilePath2() {
        return filePath2;
    }

    public void setFilePath2(Uri filePath2) {
        this.filePath2 = filePath2;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_LICENS) {

                setFilePath(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());
                setFilePath(uriFromPath);
                edt_driving_licens.setText(fileDeviceURL);

            }else {

                setFilePath2(data.getData());
                fileType = AppConst.FILE_TYPE_FILE;
                fileExtention = AppConst.FILE_EXT_IMAGE;
                fileURL = "NULL";
                Uri uriFromPath = Uri.fromFile(new File(FilePath.getPath(getActivity(), data.getData())));
                fileDeviceURL = FilePath.getPath(getActivity(), data.getData());
                setFilePath2(uriFromPath);
                edt_carownership.setText(fileDeviceURL);

            }

        }

    }

}
