package com.muverity.caber.common;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.muverity.caber.MyApplication;
import com.muverity.caber.R;
import com.muverity.caber.api.APIService;
import com.muverity.caber.api.CaberWSapi;
import com.muverity.caber.api.PreferenceUtils;
import com.muverity.caber.api.Utils;
import com.muverity.caber.customview.PinEntryEditText;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.muverity.caber.model.Otp_Valid_Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class Otp_fragment extends Fragment {

    PreferenceUtils pref;
    Button btn_submit;
    TextView tv_edit;
    EditText et_email;
    String str_token , deviceId;
    NavController navController ;
    ImageView iv_back;
    public int counter;
    PinEntryEditText txt_pin_entry;
    String phone_number ;
    FirebaseAuth mAuth;
    String mVerificationId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.otp_fragment, container, false);
    }


    @SuppressLint("ResourceType")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FirebaseApp.initializeApp(MyApplication.getAppContext());

        navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
        txt_pin_entry = (PinEntryEditText) view.findViewById(R.id.txt_pin_entry);

        mAuth = FirebaseAuth.getInstance();
        phone_number = getArguments().getString("phone_number");
        sendVerificationCode(phone_number);

        btn_submit = view.findViewById(R.id.btn_submit);
        tv_edit = view.findViewById(R.id.tv_edit);
        et_email = view.findViewById(R.id.et_email);
        iv_back = view.findViewById(R.id.iv_back);
        pref = new PreferenceUtils(getActivity());

       // str_token = FirebaseInstanceId.getInstance().getToken();
        deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        pref.settoken(deviceId);

        final TextView txt_count=view.findViewById(R.id.txt_count);

        new CountDownTimer(180000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                txt_count.setText(""+String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                txt_count.setText("done!");
            }
        }.start();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String code = txt_pin_entry.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    txt_pin_entry.setError("Enter valid code");
                    txt_pin_entry.requestFocus();
                    return;
                }

                //verifying the code entered manually
                verifyVerificationCode(code);

               /* validateOtp();*/
            }
        });

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_email.setEnabled(true);
                et_email.setFocusable(true);
                et_email.requestFocus();
                et_email.setCursorVisible(true);
            }
        });


       /* txt_pin_entry.setText(pref.getotp());

        txt_pin_entry.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().equals("1234")) {

                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(txt_pin_entry.getWindowToken(), 0);

                } else if (s.length() == "1234".length()) {

                    Toast.makeText(getActivity(), "Incorrect", Toast.LENGTH_SHORT).show();
                    txt_pin_entry.setText(null);

                }
            }
        });*/

    }


    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                txt_pin_entry.setText(code);
                //verifying the code
               // verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }


        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            //if success call apis
                        //    Toast.makeText(getActivity(), "Successfully" , Toast.LENGTH_SHORT).show();

                            String rawdata = getJson();
                            TypedInput in = null;
                            try {
                                in = new TypedByteArray("application/json", rawdata.getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }


                            if (Utils.isNetworkAvailable(getActivity())) {
                                if(Utils.emailValidator(et_email.getText().toString()) && et_email.length() != 0){
                                    Utils.showProgress1("Please wait", getActivity());
                                    CaberWSapi api = APIService.createService(CaberWSapi.class);
                                    api.validate_otp(
                                            in,
                                            new Callback<Otp_Valid_Model>() {
                                                @Override
                                                public void success(Otp_Valid_Model otp_valid_model, Response response) {
                                                    Utils.hideProgress1();

                                                    pref.setfirstname(otp_valid_model.getFirst_name());
                                                    pref.setlastname(otp_valid_model.getLast_name());
                                                    pref.setemail(otp_valid_model.getEmail());
                                                    pref.setrole(otp_valid_model.getRole());
                                                    pref.setcar_model(otp_valid_model.getCar_model());
                                                    pref.setcar_ownership_doc(otp_valid_model.getCar_ownership_doc());
                                                    pref.setcountry_code(otp_valid_model.getCountry_code());
                                                    pref.setdriver_licence(otp_valid_model.getDriver_licence());
                                                    pref.setprofile_pic(otp_valid_model.getProfile_pic());
                                                    pref.setauthtoke(otp_valid_model.getAuth_token());
                                                    pref.setlogin("1");
                                                    pref.setuserid(otp_valid_model.getId());
                                                    pref.setcartype(otp_valid_model.getCar_type());
                                                    pref.settotal_rating(otp_valid_model.getTotal_rating());
                                                    pref.gsettotal_rides(otp_valid_model.getTotal_rides());

                                                    Bundle bundle = new Bundle();
                                                    bundle.putString("key","passenger");
                                                    navController.navigate(R.id.otp_fragment_to_start_fragment , bundle);

                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                                    Utils.hideProgress1();
                                                }
                                            });

                                }else {

                                    Toast.makeText(getActivity(), "Please enter correct email id", Toast.LENGTH_SHORT).show();
                                }


                            } else {
                                Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
                            }

                            //verification successful we will start the profile activity
                           /* Intent intent = new Intent(VerifyPhoneActivity.this, ProfileActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);*/

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void validateOtp() {

        String rawdata = getJson();
        TypedInput in = null;
        try {
            in = new TypedByteArray("application/json", rawdata.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        if (Utils.isNetworkAvailable(getActivity())) {
            if(Utils.emailValidator(et_email.getText().toString()) && et_email.length() != 0){
                Utils.showProgress1("Please wait", getActivity());

                CaberWSapi api = APIService.createService(CaberWSapi.class);
                api.validate_otp(
                        in,
                        new Callback<Otp_Valid_Model>() {
                            @Override
                            public void success(Otp_Valid_Model otp_valid_model, Response response) {
                                Utils.hideProgress1();

                                pref.setfirstname(otp_valid_model.getFirst_name());
                                pref.setlastname(otp_valid_model.getLast_name());
                                pref.setemail(otp_valid_model.getEmail());
                                pref.setrole(otp_valid_model.getRole());
                                pref.setcar_model(otp_valid_model.getCar_model());
                                pref.setcar_ownership_doc(otp_valid_model.getCar_ownership_doc());
                                pref.setcountry_code(otp_valid_model.getCountry_code());
                                pref.setdriver_licence(otp_valid_model.getDriver_licence());
                                pref.setprofile_pic(otp_valid_model.getProfile_pic());
                                pref.setauthtoke(otp_valid_model.getAuth_token());
                                pref.setlogin("1");
                                pref.setuserid(otp_valid_model.getId());

                                navController.navigate(R.id.otp_fragment_to_start_fragment);

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(MyApplication.getAppContext(), "Connection Refused", Toast.LENGTH_SHORT).show();
                                Utils.hideProgress1();
                            }
                        });

            }else {

                Toast.makeText(getActivity(), "Please enter correct email id", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(getActivity(), "Please connect to an internet", Toast.LENGTH_SHORT).show();
        }
    }


    public String getJson() {
        String str_email = et_email.getText().toString();
        String[] result = str_email.split("@");
        str_email = result[0];

        JSONObject obj = new JSONObject();
        try {
            obj.put("first_name", str_email);
            obj.put("phone_number", phone_number);
            obj.put("email", et_email.getText().toString());
            obj.put("device_token",pref.gettoken());
            obj.put("device_type","android");
            obj.put("role",1);

        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return obj.toString();
    }




}
